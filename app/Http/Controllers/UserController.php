<?php

namespace App\Http\Controllers;

use App\Models\BillingDetail;
use App\Models\Company;
use App\Mailjet\Mailjet;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Kreait\Laravel\Firebase\Facades\FirebaseAuth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isAdmin', User::class);
        return User::all()->load('company')->load('billing_detail');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isAdmin', User::class);
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }
        $user = User::create($this->getUser($request));
        $company = $this->getCompany($request);
        $company = array_merge($company, ['user_id' => $user->id]);
        Company::create($company);

        if (!$user->use_company_details_for_billing) {
            $billing_detail = $this->getBillingDetail($request);
            $billing_detail = array_merge($billing_detail, ['user_id' => $user->id]);
            BillingDetail::create($billing_detail);
        }

        $user->google_id = app('firebase.auth')->createUser(['email' => $user->email, 'password' => 'mypassword'])->uid;
        $user->save();
        $this->sendWelcomeMessage($user);
        return response(['id' => $user->id], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $this->authorize('isHimSelf', $user);
        return $user->load('company')->load('billing_detail');
    }

    /**
     * Find user by google id
     */
    public function findUserByGoogleId($id)
    {
        return User::where('google_id', '=', $id)->first()->load('company')->load('billing_detail');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->authorize('isHimSelf', $user);
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }

        $user->update($this->getUser($request));
        $company = Company::where('user_id', '=', $user->id)->first();
        $company->update($this->getCompany($request));

        if (!$user->use_company_details_for_billing) {
            $billing_detail = BillingDetail::where('user_id', '=', $user->id)->first();
            if ($billing_detail) {
                $billing_detail->update($this->getBillingDetail($request));
            } else {
                $billing_detail = $this->getBillingDetail($request);
                $billing_detail = array_merge($billing_detail, ['user_id' => $user->id]);
                BillingDetail::create($billing_detail);
            }
        }
        return $user->load('company')->load('billing_detail');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin', User::class);
        return User::findOrFail($id)->delete();
    }

    /**
     * Send welcome message for a given user
     */
    private function sendWelcomeMessage($user)
    {
        $link = FirebaseAuth::getPasswordResetLink($user->email);
        Mailjet::sendWelcomeMessage(
            [
                'to' => $user->email,
                'link' => $link,
            ]
        );
    }

    /**
     * Send a password reset link for a given user
     */
    public function sendPasswordResetLink($id)
    {
        $this->authorize('isAdmin', User::class);
        $user = User::findOrFail($id);
        try {
            $link = FirebaseAuth::getPasswordResetLink($user->email);
            Mailjet::sendResetPassword(
                [
                    'to' => $user->email,
                    'link' => $link,
                ]
            );
            return response(['status' => 'sent'], 200);
        } catch (Exception $e) {
            return response(['status' => 'not sent'], 400);
        }
    }

    /**
     * Reset a password
     */
    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), $this->resetPasswordRules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }
        $input = $request->all();
        try {
            FirebaseAuth::confirmPasswordReset($input['code'], $input['password'], true);
            return response(['status' => 'ok'], 200);
        } catch (\Kreait\Firebase\Exception\Auth\ExpiredOobCode $e) {
            return response(['errors' => "$e->getMessage()"], 400);
        } catch (\Kreait\Firebase\Exception\Auth\InvalidOobCode $e) {
            return response(['errors' => $e->getMessage()], 400);
        } catch (\Kreait\Firebase\Exception\InvalidArgumentException $e) {
            return response(['errors' => $e->getMessage()], 400);
        } catch (\Kreait\Firebase\Exception\AuthException $e) {
            return response(['errors' => $e->getMessage()], 400);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    private function rules()
    {
        return array_merge($this->userRules(), $this->companyRules(), $this->billingDetailRules());
    }

    private function userRules()
    {
        return [
            'email' => 'required|email',
            'is_admin' => 'required|bool',
            'use_company_details_for_billing' => 'required|bool',
        ];
    }

    private function companyRules()
    {
        return [
            'company.name' => 'required|string',
            'company.vat_number' => 'required|string',
            'company.street' => 'required|string',
            'company.postal_code' => 'required|string',
            'company.city' => 'required|string',
        ];
    }

    private function billingDetailRules()
    {
        return [
            'billing_detail.name' => 'exclude_if:use_company_details_for_billing,true|required|string',
            'billing_detail.vat_number' => 'exclude_if:use_company_details_for_billing,true|required|string',
            'billing_detail.street' => 'exclude_if:use_company_details_for_billing,true|required|string',
            'billing_detail.postal_code' => 'exclude_if:use_company_details_for_billing,true|required|string',
            'billing_detail.city' => 'exclude_if:use_company_details_for_billing,true|required|string',
        ];
    }

    private function resetPasswordRules()
    {
        return [
            'code' => 'required|string',
            'password' => 'required|string',
        ];
    }
    private function getUser(Request $request)
    {
        return $request->validate($this->userRules());
    }

    private function getCompany(Request $request)
    {
        return $request->validate($this->companyRules())['company'];
    }

    private function getBillingDetail(Request $request)
    {
        return $request->validate($this->billingDetailRules())['billing_detail'];
    }
}
