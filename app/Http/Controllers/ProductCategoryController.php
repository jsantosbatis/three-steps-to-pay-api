<?php

namespace App\Http\Controllers;

use App\Models\ProductCategory;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ProductCategory::all();
    }

    public function fromLang($lang)
    {
        $items = ProductCategory::all('id', 'name_' . $lang);
        return collect($items)->map(function ($item) use ($lang) {
            return [
                'id' => $item->id,
                'name' => $item->{'name_' . $lang}
            ];
        });
    }
}
