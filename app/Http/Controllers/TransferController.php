<?php

namespace App\Http\Controllers;

use App\Http\Resources\TransferResource;
use App\Models\Transfer;
use App\Models\User;
use Illuminate\Http\Request;

class TransferController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function treated(Transfer $transfer, User $user)
    {
        $this->authorize('isAdmin', $user);
        $transfer->update(['treated' => true]);

        return new TransferResource($transfer);
    }
}
