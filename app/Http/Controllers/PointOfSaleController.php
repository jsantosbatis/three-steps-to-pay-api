<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductResourceCollection;
use App\Http\Resources\store\PointOfSaleResource;
use App\Http\Resources\store\PointOfSaleResourceCollection;
use App\Http\Resources\store\PointOfSaleWithProductsResource;
use App\Http\Resources\TotalSalesResource;
use App\Http\Resources\TransactionResource;
use App\Http\Resources\TransactionResourceCollection;
use App\Http\Resources\TotalSalesCategoryResource;
use App\Http\Resources\TotalSalesCategoryResourceCollection;
use App\Models\PointOfSale;
use App\Models\Transaction;
use Carbon\Carbon;
use DateTime;
use Endroid\QrCode\QrCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class PointOfSaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = PointOfSale::select(DB::raw('point_of_sales.*, count(*) as count_products'))
            ->leftJoin('products', 'products.point_of_sale_id', '=', 'point_of_sales.id')
            ->where('user_id', '=', Auth::user()->id)
            ->groupBy('point_of_sales.id')
            ->get();
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        };

        $pointOfSale = PointOfSale::create(array_merge($request->all(), ['user_id' => Auth::user()->id]));

        return response(['id' => $pointOfSale->id], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, PointOfSale $pointOfSale)
    {
        $this->authorize('isOwner', $pointOfSale);
        return $pointOfSale->append('count_products');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PointOfSale $pointOfSale)
    {
        $this->authorize('isOwner', $pointOfSale);
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }
        $pointOfSale->update(array_merge($request->all(), ['user_id' => $request->user_id]));
        return $pointOfSale->append('count_products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PointOfSale $pointOfSale)
    {
        $this->authorize('isOwner', $pointOfSale);
        $pointOfSale->load('products');
        //Deactivate if it contains products
        if (count($pointOfSale->products) > 0) {
            return $pointOfSale->delete();
        } else {
            return $pointOfSale->forceDelete();
        }
    }

    /**
     * Return the products belonging for the given point of sale
     *
     * @return array
     */
    public function products(Request $request, PointOfSale $pointOfSale)
    {
        if (!$request->is('api/app/*')) {
            $this->authorize('isOwner', $pointOfSale);
        }
        return new ProductResourceCollection(ProductResource::collection($pointOfSale->products));
    }

    /**
     * Return sales for a given period
     */
    public function lastSales(Request $request, PointOfSale $pointOfSale)
    {
        $this->authorize('isOwner', $pointOfSale);
        $input = $request->all();
        $validator = Validator::make($input, $this->byPeriodRules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }

        $start_date = new Carbon(new DateTime($input['start_date']));
        $end_date = new Carbon(new DateTime($input['end_date']));
        $result = Transaction::select(
            'transactions.*',
            'products.name as product_name',
            'point_of_sales.name as point_of_sale_name',
            'payment_method_types.name as payment_method_type_name'
        )
            ->join('products', 'products.id', '=', 'transactions.product_id')
            ->join('point_of_sales', 'point_of_sales.id', '=', 'products.point_of_sale_id')
            ->join('payment_method_types', 'payment_method_types.id', '=', 'transactions.payment_method_type_id')
            ->whereBetween('transactions.date', [$start_date, $end_date])
            ->where('point_of_sales.user_id', '=', Auth::user()->id)
            ->where('point_of_sales.id', '=', $pointOfSale->id)
            ->orderBy('date', 'desc')
            ->get();

        return new TransactionResourceCollection(TransactionResource::collection(map_transaction_sales($result)));
    }

    /**
     * Return total sales for a given period
     */
    public function totalSales(Request $request, PointOfSale $pointOfSale)
    {
        $this->authorize('isOwner', $pointOfSale);
        $input = $request->all();
        $validator = Validator::make($input, $this->byPeriodRules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }
        $start_date = new Carbon(new DateTime($input['start_date']));
        $end_date = new Carbon(new DateTime($input['end_date']));
        return new TotalSalesResource(Transaction::select(
            DB::raw('IFNULL(sum(transactions.amount),0) as amount, 
                     IFNULL(sum(transactions.net_amount),0) as net_amount,
                     IFNULL(count(*) ,0) as total ')
        )
            ->join('products', 'products.id', '=', 'transactions.product_id')
            ->join('point_of_sales', 'point_of_sales.id', '=', 'products.point_of_sale_id')
            ->whereBetween('transactions.created_at', [$start_date, $end_date])
            ->where('point_of_sales.user_id', '=', Auth::user()->id)
            ->where('point_of_sales.id', '=', $pointOfSale->id)
            ->first());
    }


    /**
     * Return sales (total, total net amount, date) per product category for a given period
     */
    public function totalSalesCategory(Request $request, PointOfSale $pointOfSale)
    {
        $this->authorize('isOwner', $pointOfSale);
        $input = $request->all();
        $validator = Validator::make($input, $this->byPeriodRules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }
        $start_date = new Carbon(new DateTime($input['start_date']));
        $end_date = new Carbon(new DateTime($input['end_date']));
        return new TotalSalesCategoryResourceCollection(TotalSalesCategoryResource::collection(
            DB::select(
                '
                SELECT COUNT(transactions.id) total, product_categories.name_fr product_category_name,
                    SUM(transactions.net_amount) net_amount, CAST(transactions.created_at AS date) date
                FROM transactions
                INNER JOIN products ON products.id = transactions.product_id
                INNER JOIN product_categories ON product_categories.id = products.product_category_id
                WHERE (CAST(transactions.created_at AS date) BETWEEN ? AND ?) AND products.point_of_sale_id = ?
                GROUP BY products.product_category_id, CAST(transactions.created_at AS date)',
                [$start_date, $end_date, $pointOfSale->id]
            )
        ));
    }

    public function totalSalesPaymentMethodType(Request $request, PointOfSale $pointOfSale)
    {
        $this->authorize('isOwner', $pointOfSale);
        $input = $request->all();
        $validator = Validator::make($input, $this->byPeriodRules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }
        $start_date = new Carbon(new DateTime($input['start_date']));
        $end_date = new Carbon(new DateTime($input['end_date']));
        return DB::select(
            '
            SELECT payment_method_types.name payment_method_type_name, 
                COUNT(transactions.payment_method_type_id) total
            FROM transactions
            INNER JOIN products ON products.id = transactions.product_id
            INNER JOIN payment_method_types ON payment_method_types.id = transactions.payment_method_type_id
            WHERE (CAST(transactions.date AS DATE) BETWEEN ? AND ?) AND products.point_of_sale_id = ?
            GROUP BY transactions.payment_method_type_id',
            [$start_date, $end_date, $pointOfSale->id]
        );
    }

    public function generateQRCode(PointOfSale $pointOfSale)
    {
        $url = env('APP_FRONT_URL') . 'store/' . $pointOfSale->token;
        return generate_qr_code($url, $pointOfSale->name, '');
    }

    public function findGeo(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, $this->findGeoRules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }

        //6371 form KM
        //Code from https://stackoverflow.com/questions/37876166/haversine-distance-calculation-between-two-points-in-laravel
        $haversine = "(6371 * acos(cos(radians(" . $input['lat'] . " )) " .
            "* cos(radians(point_of_sales.lat)) " .
            "* cos(radians(point_of_sales.lng) " .
            "- radians(" . $input['lng'] . ")) " .
            "+ sin(radians(" . $input['lat'] . ")) " .
            "* sin(radians(point_of_sales.lat)))) distance";

        $result = DB::select(
            'select * 
            from (
                select point_of_sales.id, point_of_sales.token, point_of_sales.name , point_of_sales.street, point_of_sales.city, point_of_sales.postal_code, ' . $haversine . '
                from point_of_sales
                where point_of_sales.deleted_at is null
            ) search
            where search.distance < 5'
        );
        return new PointOfSaleResourceCollection(PointOfSaleResource::collection($result));
    }

    public function search(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, $this->searchRules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }
        return new PointOfSaleResourceCollection(PointOfSaleResource::collection(PointOfSale::select('id', 'token', 'name', 'street', 'city', 'postal_code')
            ->where('name', 'like', '%' . $input['name'] . '%')->orWhere('token', '=', $input['name'])->orderBy('postal_code')->get()));
    }

    public function getPointOfSaleFromToken($token)
    {
        $pointOfSale = PointOfSale::where('token', '=', $token)->firstOrFail();
        $pointOfSale->load('products');
        return new PointOfSaleWithProductsResource($pointOfSale);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    private function rules()
    {
        return [
            'name' => 'required|string',
            'postal_code' => 'required|string',
            'city' => 'required|string',
            'bank_account' => 'required|string'
        ];
    }

    private function byPeriodRules()
    {
        return [
            'start_date' => 'required|date ',
            'end_date' => 'required|date '
        ];
    }

    private function findGeoRules()
    {
        return [
            'lat' => 'required|numeric ',
            'lng' => 'required|numeric '
        ];
    }

    private function searchRules()
    {
        return [
            'name' => 'string',
        ];
    }
}
