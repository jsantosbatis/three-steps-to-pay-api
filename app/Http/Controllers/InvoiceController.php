<?php

namespace App\Http\Controllers;

use App\Http\Resources\InvoiceResource;
use App\Http\Resources\InvoiceResourceCollection;
use App\Http\Resources\TransferResource;
use App\Http\Resources\TransferResourceCollection;
use App\Models\Invoice;

use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, $this->rules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }
        $start_date = new Carbon(new DateTime($input['start_date']));
        $end_date = new Carbon(new DateTime($input['end_date']));
        return new InvoiceResourceCollection(InvoiceResource::collection(
            Invoice::where('user_id', '=', Auth::user()->id)
                ->whereBetween('created_at', [$start_date, $end_date])
                ->get()
        ));
    }

    public function transfers(Invoice $invoice)
    {
        $this->authorize('isOwner', $invoice);
        $invoice->load('transfers');
        return  new TransferResourceCollection(TransferResource::collection($invoice->transfers));
    }

    public function download(Invoice $invoice)
    {
        $this->authorize('isOwner', $invoice);
        $storage = app('firebase.storage');
        $bucket = $storage->getBucket();
        $object = $bucket->object($invoice->file_path);
        $file_name = array_reverse(explode('/', $object->name()))[0];
        return  response($object->downloadAsStream()->getContents(), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'attachment; filename="' . $file_name . '.pdf"',
        ]);
    }

    private function rules()
    {
        return [
            'start_date' => 'required|date ',
            'end_date' => 'required|date '
        ];
    }
}
