<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Http\Resources\store\ProductWithPointOfSaleResource;
use App\Models\Product;
use App\QrCodeApi\QrCodeApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        };
        $product = Product::create(array_merge($request->all(), ['price' => remove_decimal_point($request->all()['price'])]));

        return response(['id' => $product->id], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Product $product)
    {
        $this->authorize('isOwner', $product);
        return new ProductResource($product->append('secondary_bank_account'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->authorize('isOwner', $product);
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }
        $product->update(array_merge($request->all(), ['price' => remove_decimal_point($request->all()['price'])]));
        return new ProductResource($product->append('secondary_bank_account'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->authorize('isOwner', $product);
        return $product->delete();
    }

    public function generateQRCode(Product $product)
    {
        $url = env('APP_FRONT_URL') . 'store/direct/' . $product->token;
        $text = $product->name . ' ' . int_to_euro($product->price);
        return generate_qr_code($url, $product->pointOfSale->name, $text);
    }

    public function generatePayconiqQRCode(Product $product)
    {
        $text = $product->name . ' ' . int_to_euro($product->price);
        return generate_payconiq_frame($product->qr_code_url, $product->pointOfSale->name, $text);
    }

    public function getProductFromToken($token)
    {
        return new ProductWithPointOfSaleResource(Product::where('token', '=', $token)->first());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    private function rules()
    {
        return [
            'name' => 'required|string',
            'price' => 'required|numeric',
            'point_of_sale_id' => 'required|integer',
            'product_category_id' => 'required|integer',
            'main_percentage' => 'required',
        ];
    }
}
