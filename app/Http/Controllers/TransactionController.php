<?php

namespace App\Http\Controllers;

use DateTime;
use Carbon\Carbon;
use App\Models\Transaction;
use App\Models\Product;
use App\Models\PaymentRequest;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\TotalSalesResource;
use App\Http\Resources\TransactionResource;
use App\Http\Resources\TransactionResourceCollection;
use App\Models\PointOfSale;
use App\QrCodeApi\QrCodeApi;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Return total sales for a given period
     */
    public function totalSales(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, $this->totalSalesRules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }
        $start_date = new Carbon(new DateTime($input['start_date']));
        $end_date = (new Carbon(new DateTime($input['end_date'])))->endOfDay();

        return new TotalSalesResource(Transaction::select(DB::raw('IFNULL(sum(transactions.amount),0) as amount, IFNULL(count(*), 0) as total, IFNULL(sum(transactions.net_amount), 0) as net_amount'))
            ->join('products', 'products.id', '=', 'transactions.product_id')
            ->join('point_of_sales', 'point_of_sales.id', '=', 'products.point_of_sale_id')
            ->whereBetween('transactions.created_at', [$start_date, $end_date])
            ->where('point_of_sales.user_id', '=', $request->user_id)
            ->first());
    }

    /**
     * Return last sales occured in 24 hours
     */
    public function lastSales(Request $request)
    {
        $now = Carbon::now();
        $last24 = Carbon::now()->subHours(24);
        $result = Transaction::select(
            'transactions.*',
            'products.name as product_name',
            'point_of_sales.name as point_of_sale_name',
            'payment_method_types.name as payment_method_type_name'
        )
            ->join('products', 'products.id', '=', 'transactions.product_id')
            ->join('point_of_sales', 'point_of_sales.id', '=', 'products.point_of_sale_id')
            ->join('payment_method_types', 'payment_method_types.id', '=', 'transactions.payment_method_type_id')
            ->whereBetween('transactions.date', [$last24, $now])
            ->where('point_of_sales.user_id', '=', $request->user_id)
            ->orderBy('date', 'desc')
            ->limit(10)
            ->get();
        return new TransactionResourceCollection(TransactionResource::collection(map_transaction_sales($result)));
    }

    public function checkout(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, $this->checkoutRules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }

        $token = Str::random(35);
        create_payment_request($input['items'], $token, config('constants.PAYMENT_METHOD_TYPE_ID_PAYCONIQ'));

        $pointOfSaleId = $input['items'][0]['point_of_sale_id'];
        $pointOfSale = PointOfSale::with('user')->where('id', '=', $pointOfSaleId)->first();

        //Map products for PAGI API
        $products = collect($input['items'])->map(function ($product) {
            return [
                'id' => $product['id'],
                'name' => $product['name'],
                'amount' => remove_decimal_point($product['price']),
            ];
        });

        $products = map_items($products);
        $checkout = QrCodeApi::checkout([
            'products' => $products,
            'token' => $token,
            'pagi_app_id' => $pointOfSale['user']['pagi_app_id'],
            'lang' => $input['lang']
        ]);
        return response()->json([
            'checkout_url' => $checkout['url']
        ], 201);
    }


    public function callback(Request $request)
    {
        $transaction = json_decode($request->getContent(), true);

        $token = $transaction['customerReference'];
        $product = Product::where('token', $token)->first();
        if ($product) { // In store payment through Static QR Code
            Transaction::create([
                'amount' => $transaction['amount'],
                'net_amount' => $transaction['netAmount'],
                'fee' => $transaction['fee'],
                'date' => Carbon::parse($transaction['succeededAt']),
                'payment_id' => $transaction['providerReference'],
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'in_store' => true,
                'payment_method_type_id' => translate_payment_method_type($transaction['paymentMethodType'])
            ]);
        } else {
            $paymentRequests = PaymentRequest::where('token', $token)->orderBy('amount', 'desc')->get();
            if (count($paymentRequests) === 0)
                return;
            $rest_fee = $transaction['fee'];
            foreach ($paymentRequests as $payment) {
                if ($rest_fee > 0) {
                    $net_amount = $payment['amount'] - $rest_fee;
                    if ($net_amount < 0) {
                        $fee = $payment['amount'];
                        $rest_fee = abs($net_amount);
                        $net_amount = 0;
                    } else {
                        $fee = $rest_fee;
                        $rest_fee = 0;
                    }
                } else {
                    $net_amount = $payment['amount'];
                    $fee = 0;
                }

                Transaction::create([
                    'amount' => $payment['amount'],
                    'net_amount' => $net_amount,
                    'fee' => $fee,
                    'date' => Carbon::parse($transaction['succeededAt']),
                    'payment_id' => $token,
                    'status' => 'SUCCEEDED',
                    'product_id' => $payment['product_id'],
                    'in_store' => false,
                    'payment_method_type_id' => translate_payment_method_type($transaction['paymentMethodType'])
                ]);
            }
            PaymentRequest::where('token', $token)->delete();
        }
    }

    private function checkoutRules()
    {
        return [
            'items.*.id' => 'required|integer',
            'items.*.price' => 'required|numeric',
            'items.*.name' => 'required|string',
            'items.*.point_of_sale_id' => 'required|integer',
            'lang' => 'required|string'
        ];
    }

    private function totalSalesRules()
    {
        return [
            'start_date' => 'required|date ',
            'end_date' => 'required|date '
        ];
    }
}
