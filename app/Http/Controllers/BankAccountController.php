<?php

namespace App\Http\Controllers;

use App\Models\BankAccount;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return BankAccount::where('user_id', Auth::user()->id)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        };

        $bankAccount = BankAccount::create(array_merge($request->all(), ['user_id' => Auth::user()->id]));

        return response(['id' => $bankAccount->id], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function show(BankAccount $bankAccount)
    {
        $this->authorize('isOwner', $bankAccount);
        return $bankAccount;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BankAccount $bankAccount)
    {
        $this->authorize('isOwner', $bankAccount);
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }
        $bankAccount->update(array_merge($request->all(), ['user_id' => $request->user_id]));
        return $bankAccount;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function destroy(BankAccount $bankAccount)
    {
        $this->authorize('isOwner', $bankAccount);
        $secondaryCount = Product::where('secondary_bank_account_id', '=', $bankAccount->id)->get()->count();
        if ($secondaryCount == 0) {
            return $bankAccount->delete();
        } else {
            return response(['message' => 'Could not delete the bank account'], 400);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    private function rules()
    {
        return [
            'name' => 'required|string',
            'number' => 'required|string',
        ];
    }
}
