<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Exception;
use Firebase\Auth\Token\Exception\InvalidToken;
use Illuminate\Support\Facades\Auth;
use Kreait\Laravel\Firebase\Facades\FirebaseAuth;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->bearerToken())
            return response(['error' => 'Token is missing'], 401, [
                'Accept' => 'application/json'
            ]);
        try {
            $google_id = $request->bearerToken();
            if (env('APP_ENV') != 'testing') {
                $google_id = FirebaseAuth::verifyIdToken($request->bearerToken())->claims()->get('sub');
            }
            $user = User::where('google_id', '=', $google_id)->first();
            $request->request->set('user_id', $user->id);
            Auth::login($user);
            return $next($request);
        } catch (InvalidToken $e) {
            return response(['error' => 'Invalid token'], 401, [
                'Accept' => 'application/json'
            ]);
        } catch (Exception $e) {
            return response(['error' => $e->getMessage()], 401, [
                'Accept' => 'application/json'
            ]);
        }
    }
}
