<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'amount' => int_to_euro($this->amount),
            'sales_amount' => int_to_euro($this->sales_amount),
            'created_at' => $this->created_at,
            'file_path' => $this->file_path,
            'user_id' => $this->user_id,
            'number' => $this->number,
        ];
    }
}
