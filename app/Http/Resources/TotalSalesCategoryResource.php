<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TotalSalesCategoryResource  extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'total' => $this->total,
            'product_category_name' => $this->product_category_name,
            'net_amount' => int_to_decimal($this->net_amount),
            'date' => Carbon::parse($this->date)->format('d-m-Y')
        ];
    }
}
