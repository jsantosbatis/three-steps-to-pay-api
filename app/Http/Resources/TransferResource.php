<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'amount' => int_to_euro($this->amount),
            'communication' => $this->communication,
            'treated' => $this->treated,
            'bank_account' => $this->bank_account,
            'bank_name' => $this->bank_name,
            'product_name' => $this->product->name,
        ];
    }
}
