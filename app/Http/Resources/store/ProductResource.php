<?php

namespace App\Http\Resources\store;

use App\Models\ProductCategory;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => floatval(int_to_decimal($this->price)),
            'point_of_sale_id' => $this->point_of_sale_id,
            'product_category_name' => ProductCategory::find($this->product_category_id)->name_en,
            'product_category_id' => $this->product_category_id
        ];
    }
}
