<?php

namespace App\Http\Resources\store;

use Illuminate\Http\Resources\Json\JsonResource;

class PointOfSaleWithProductsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'token' => $this->token,
            'name' => $this->name,
            'street' =>  $this->street,
            'city' => $this->city,
            'postal_code' => $this->postal_code,
            'products' => new ProductResourceCollection(ProductResource::collection($this->products))
        ];
    }
}
