<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'point_of_sale_name' => $this['point_of_sale_name'],
            'product_name' => $this['product_name'] ?? '',
            'product_count' => $this['product_count'],
            'amount' => int_to_decimal($this['amount']),
            'net_amount' => int_to_decimal($this['net_amount']),
            'fee' => int_to_decimal($this['fee']),
            'date' => $this['date'],
            'payment_method_type_id' => $this['payment_method_type_id'],
            'payment_method_type_name' => $this['payment_method_type_name']
        ];
    }
}
