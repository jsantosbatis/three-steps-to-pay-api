<?php

namespace App\Http\Resources;

use App\Models\ProductCategory;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => int_to_decimal($this->price),
            'qr_code_url' => $this->qr_code_url,
            'main_percentage' => $this->main_percentage,
            'secondary_bank_account_id' => $this->secondary_bank_account_id,
            'secondary_percentage' => $this->secondary_percentage,
            'point_of_sale_id' => $this->point_of_sale_id,
            'product_category_id' => $this->product_category_id,
            'product_category_name' => ProductCategory::find($this->product_category_id)->name_fr
        ];
    }
}
