<?php

namespace App\Jobs;

use App\Models\Invoice;
use App\Mailjet\Mailjet;
use App\Models\Transfer;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class SendInvoice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;
    public $timeout = 20;
    private $params = [];
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params = [])
    {
        $defaults = [
            'user_id' => '',
            'start_date' => '',
            'end_date' => ''
        ];
        $this->params = array_merge($defaults, $params);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->generate($this->params['user_id'], $this->params['start_date'], $this->params['end_date']);
    }

    /**
     * Generate an invoice
     */
    public function generate($user_id, $start_date, $end_date)
    {
        $start_date = new Carbon(new DateTime($start_date));
        $end_date = new Carbon(new DateTime($end_date));
        $end_date->endOfDay();
        $report_data = [];
        $complement = [
            "start_date" =>  $start_date->toDateString(),
            "end_date" => $end_date->toDateString(),
            "invoice_number" => Invoice::getNextNumber(),
            "invoice_date" => Carbon::now()->toDateString()
        ];
        $total_sales_amount = 0;
        $total_fees_amount = 0;
        $total_gross_amount = 0;

        $user = User::findOrFail($user_id)->load('company')->load('billing_detail')->toArray();
        $company = $user['billing_detail'] ? $user['billing_detail'] : $user['company'];
        $result = DB::select(
            'SELECT point_of_sales.id pos_id, products.id pro_id, point_of_sales.name pos_name, 
                products.name pro_name, COUNT(products.id) pro_count, products.price,
	            SUM(transactions.amount) amount, SUM(transactions.fee) fee, SUM(transactions.net_amount) net_amount,
                point_of_sales.name main_bank_name, point_of_sales.bank_account main_bank, products.main_percentage,
                ROUND(SUM(transactions.net_amount) * (products.main_percentage /100)) main_net_amount,
                bank_accounts.name sec_bank_name, products.secondary_percentage sec_percentage,
                bank_accounts.number sec_bank, 
                ROUND(SUM(transactions.net_amount) * (products.secondary_percentage /100)) sec_net_amount
            FROM transactions
            INNER JOIN products ON products.id = transactions.product_id
            INNER JOIN point_of_sales ON point_of_sales.id = products.point_of_sale_id
            INNER JOIN users ON users.id = point_of_sales.user_id
            LEFT JOIN bank_accounts ON bank_accounts.id = products.secondary_bank_account_id
            LEFT JOIN transaction_transfer ON transactions.id = transaction_transfer.transaction_id
            WHERE transactions.date <= ?
                AND transaction_transfer.transaction_id IS NULL
                AND point_of_sales.user_id = ? 
            GROUP BY point_of_sales.id, products.id',
            [$end_date, $user_id]
        );
        //No transactions, no invoice
        if (count($result) <= 0) {
            return;
        }
        $data = [];
        $i = 0;
        while ($i < count($result)) {
            $crt_pos_id = $result[$i]->pos_id;
            $pos = [
                'name' => $result[$i]->pos_name,
                'gross_amount' => 0,
                'net_amount' => 0,
                'products' => [],
                'count' => 0
            ];
            while ($i < count($result) && $crt_pos_id == $result[$i]->pos_id) {
                $crt_pro_id = $result[$i]->pro_id;
                while ($i < count($result) && $crt_pos_id == $result[$i]->pos_id && $crt_pro_id == $result[$i]->pro_id) {
                    $pro = [
                        'name' => $result[$i]->pro_name,
                        'price' => int_to_euro($result[$i]->price),
                        'net_amount' => int_to_euro($result[$i]->net_amount),
                        'count' => $result[$i]->pro_count,
                        'dispatch' => []
                    ];
                    $dispatch = [[
                        'name' => $result[$i]->main_bank_name,
                        'bank_account' => $result[$i]->main_bank,
                        'percentage' => $result[$i]->main_percentage,
                        'net_amount' => int_to_euro($result[$i]->main_net_amount)
                    ]];
                    if ($result[$i]->sec_bank_name) {
                        array_push($dispatch, [
                            'name' => $result[$i]->sec_bank_name,
                            'bank_account' => $result[$i]->sec_bank,
                            'percentage' => $result[$i]->sec_percentage,
                            'net_amount' => int_to_euro($result[$i]->sec_net_amount)
                        ]);
                    }
                    $pro['dispatch'] = $dispatch;
                    $pos['count']++;
                    $pos['net_amount'] += $result[$i]->net_amount;
                    $pos['gross_amount'] += $result[$i]->amount;

                    $total_fees_amount += $result[$i]->fee;
                    array_push($pos['products'], $pro);
                    $i++;
                }
            }
            $total_sales_amount += $pos['net_amount'];
            $total_gross_amount += $pos['gross_amount'];
            $pos['net_amount'] = int_to_euro($pos['net_amount']);
            array_push($data, $pos);
        }

        $to_pay_hvat = round($total_gross_amount * 0.07);
        $to_pay_vat = round($to_pay_hvat * 0.21);
        $to_pay = round($to_pay_vat + $to_pay_hvat);

        $billResume = [
            [
                'text' => 'Total des frais de transaction',
                'total_hvat' => int_to_euro($total_fees_amount),
                'pourcentage_vat' => '0%',
                'vat' => int_to_euro(0),
                'total' => int_to_euro($total_fees_amount)
            ],
            [
                'text' => 'Retrait des frais de transaction',
                'total_hvat' => int_to_euro($total_fees_amount),
                'pourcentage_vat' => '0%',
                'vat' => int_to_euro(0),
                'total' => int_to_euro($total_fees_amount)
            ],
            [
                'text' => 'Système ChurchPay',
                'total_hvat' => int_to_euro($to_pay_hvat),
                'pourcentage_vat' => '21%',
                'vat' => int_to_euro($to_pay_vat),
                'total' => int_to_euro($to_pay)
            ]
        ];

        $report_data = [
            'company' => [
                'name' => $company['name'],
                'vat_number' => $company['vat_number'],
                'street' => $company['street'],
                'postal_code' => $company['postal_code'],
                'city' =>  $company['city']
            ],
            'data' => $data,
            'bill' =>  $billResume,
            'general' => [
                'to_pay_vat' => int_to_euro($to_pay_vat),
                'to_pay' => int_to_euro($to_pay),
                'communication' => $complement['invoice_number']
            ]
        ];
        $report_options = [
            'data' => $report_data,
            'convertTo' => 'pdf',
            'complement' => $complement
        ];
        $response = Http::withToken(env('CARBON_API_KEY'))
            ->post(
                'https://render.carbone.io/render/6292bd392d96dab035963ea44f2b6cdcc416ae035f4aade561c74bfcbcafae7d',
                $report_options
            );

        $render_id = $response->json()['data']['renderId'];
        $response = Http::get('https://render.carbone.io/render/' . $render_id, ['stream' => true]);
        $storage = app('firebase.storage');
        $bucket = $storage->getBucket();
        $file_path  = 'invoice/' . $render_id;
        $bucket->upload($response->getBody()->getContents(), [
            'name' => $file_path
        ]);

        $invoice = Invoice::create([
            'start_date' => $start_date->toDateString(),
            'end_date' => $end_date->toDateString(),
            'amount' => $to_pay,
            'sales_amount' => $total_sales_amount,
            'file_path' =>  $file_path,
            'user_id' => $user_id,
            'number' => Invoice::getNextNumber()
        ]);

        $this->createTransfer($end_date, $user_id, $invoice->id);

        Mailjet::sendInvoice(
            [
                'to' => $user['email'],
                'attachment' => $file_path,
                'invoice_number' => $invoice->number,
                'month' => Str::ucfirst((new Carbon($start_date))->locale('fr_FR')->monthName)
            ]
        );
    }

    private function createTransfer($end_date, $user_id, $invoice_id)
    {
        $result = DB::select(
            'SELECT transactions.product_id, products.name product_name, 
                point_of_sales.name point_of_sale_name,
                point_of_sales.bank_account main_bank,
                point_of_sales.name main_bank_name,
                ROUND(SUM(transactions.net_amount) * (products.main_percentage /100)) main_net_amount,
                bank_accounts.name sec_bank_name,
                bank_accounts.number sec_bank,
                ROUND(SUM(transactions.net_amount) * (products.secondary_percentage /100)) sec_net_amount
            FROM transactions
            LEFT JOIN transaction_transfer ON transactions.id = transaction_transfer.transaction_id
            INNER JOIN products ON products.id = transactions.product_id
            INNER JOIN point_of_sales ON point_of_sales.id = products.point_of_sale_id
            LEFT JOIN bank_accounts ON bank_accounts.id = products.secondary_bank_account_id
            WHERE transaction_transfer.transaction_id IS NULL 
                AND transactions.date <= ?
                AND point_of_sales.user_id = ?
            GROUP BY transactions.product_id',
            [$end_date, $user_id]
        );

        foreach ($result as $item) {
            $transactionsIds = DB::select('SELECT transactions.id
                FROM transactions
                LEFT JOIN transaction_transfer ON transactions.id = transaction_transfer.transaction_id
                INNER JOIN products ON products.id = transactions.product_id
                WHERE transaction_transfer.transaction_id IS NULL 
                    AND transactions.date <= ?
                    AND products.id = ?', [$end_date, $item->product_id]);
            $transactionsIds = Arr::pluck($transactionsIds, 'id');
            $transfer = Transfer::create([
                'amount' => $item->main_net_amount,
                'communication' => 'CHURCHPAY - ' . $item->point_of_sale_name . ' - ' . $item->product_name,
                'treated' => false,
                'bank_name' => $item->main_bank_name,
                'bank_account' => $item->main_bank,
                'invoice_id' => $invoice_id,
                'product_id' => $item->product_id,
            ]);
            $transfer->transactions()->sync($transactionsIds);
            if ($item->sec_bank) {
                $transfer = Transfer::create([
                    'amount' => $item->sec_net_amount,
                    'communication' => 'CHURCHPAY - ' . $item->point_of_sale_name . ' - ' . $item->sec_bank_name . ' - ' . $item->product_name,
                    'treated' => false,
                    'bank_name' => $item->sec_bank_name,
                    'bank_account' => $item->sec_bank,
                    'invoice_id' => $invoice_id,
                    'product_id' => $item->product_id,
                ]);
                $transfer->transactions()->sync($transactionsIds);
            }
        }
    }
}
