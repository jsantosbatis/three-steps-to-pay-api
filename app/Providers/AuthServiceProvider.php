<?php

namespace App\Providers;

use App\Models\Invoice;
use App\Models\PointOfSale;
use App\Policies\InvoicePolicy;
use App\Policies\PointOfSalePolicy;
use App\Policies\ProductPolicy;
use App\Policies\UserPolicy;
use App\Models\Product;
use App\Models\User;
use App\Policies\TransferPolicy;
use App\Models\Transfer;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        PointOfSale::class => PointOfSalePolicy::class,
        Product::class => ProductPolicy::class,
        Invoice::class => InvoicePolicy::class,
        Transfer::class => TransferPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
