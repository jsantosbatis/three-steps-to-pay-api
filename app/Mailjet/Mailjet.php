<?php

namespace App\Mailjet;

use \Mailjet\Resources;

class Mailjet
{
    public static function getClient($params = [])
    {
        $defaults = ['version' => 'v3.1'];
        $params = array_merge($defaults, $params);
        return new \Mailjet\Client(env('MJ_APIKEY_PUBLIC'), env('MJ_APIKEY_PRIVATE'), true,  $params);
    }
    public static function sendInvoice($params)
    {
        $defaults = ['to' => '', 'attachment' => '', 'invoice_number' => '', 'month' => ''];
        $params = array_merge($defaults, $params);
        $storage = app('firebase.storage');
        $bucket = $storage->getBucket();
        $object = $bucket->object($params['attachment']);
        $file = $object->downloadAsString();
        $mj = self::getClient();
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => env('MJ_FROM'),
                        'Name' => "ChurchPay"
                    ],
                    'To' => [
                        [
                            'Email' => $params['to'],
                        ]
                    ],
                    'TemplateID' => 1476772,
                    'TemplateLanguage' => true,
                    'Variables' =>  [
                        'month' => $params['month'],
                        'invoice' => $params['invoice_number'],
                    ],
                    'Subject' => 'Facture - ' . $params['month'],
                    "Attachments" => [
                        [
                            'ContentType' => "application/pdf",
                            'Filename' => $params['invoice_number'] . ".pdf",
                            'Base64Content' => base64_encode($file)
                        ]
                    ]
                ]
            ]
        ];
        $mj->post(Resources::$Email, ['body' => $body]);
    }

    public static function sendWelcomeMessage($params)
    {
        $defaults = ['to' => '', 'link' => ''];
        $params = array_merge($defaults, $params);
        $mj = self::getClient();
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => env('MJ_FROM'),
                        'Name' => "ChurchPay"
                    ],
                    'To' => [
                        [
                            'Email' => $params['to'],
                        ]
                    ],
                    'TemplateID' => 1498928,
                    'TemplateLanguage' => true,
                    'Variables' =>  [
                        'link' => $params['link']
                    ],
                    'Subject' => 'Bienvenue',
                ]
            ]
        ];
        $mj->post(Resources::$Email, ['body' => $body]);
    }

    public static function sendResetPassword($params)
    {
        $defaults = ['to' => '', 'link' => ''];
        $params = array_merge($defaults, $params);
        $mj = self::getClient();
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => env('MJ_FROM'),
                        'Name' => "ChurchPay"
                    ],
                    'To' => [
                        [
                            'Email' => $params['to'],
                        ]
                    ],
                    'TemplateID' => 1498952,
                    'TemplateLanguage' => true,
                    'Variables' =>  [
                        'link' => $params['link']
                    ],
                    'Subject' => 'Réinitialisation mot de passe',
                ]
            ]
        ];
        $mj->post(Resources::$Email, ['body' => $body]);
    }

    public static function sendContactForm($params)
    {
        $defaults = ['name' => '', 'email' => '', 'phone' => '', 'message' => ''];
        $params = array_merge($defaults, $params);
        $mj = self::getClient();
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => env('MJ_FROM'),
                        'Name' => "ChurchPay"
                    ],
                    'To' => [
                        [
                            'Email' => env('MJ_FROM'),
                        ]
                    ],
                    'Variables' =>  [
                        'name' => $params['name'],
                        'email' => $params['email'],
                        'phone' => $params['phone'],
                        'message' => $params['message']
                    ],
                    'TemplateLanguage' => true,
                    'Subject' => "Contact Form",
                    'HTMLPart' => "<h1>Formulaire de contact</h1><p><strong>Name:</strong> {{var:name}}</p><p><strong>Email:</strong> {{var:email}}</p><p><strong>Phone:</strong> {{var:phone}}</p><p><strong>Message:</strong> {{var:message}}</p>"
                ]
            ]
        ];
        $mj->post(Resources::$Email, ['body' => $body]);
    }
}
