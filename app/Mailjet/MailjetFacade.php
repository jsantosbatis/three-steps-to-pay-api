<?php

namespace App\Mailjet;

use Illuminate\Support\Facades\Facade;

class MailjetFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'mailjet';
    }
}
