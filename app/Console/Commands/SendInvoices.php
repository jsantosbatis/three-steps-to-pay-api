<?php

namespace App\Console\Commands;

use App\Jobs\SendInvoice;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send invoices to the users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        foreach ($users as $user) {
            $start = new Carbon('first day of last month');
            $start_date = $start->toDateString();
            $end = new Carbon('last day of last month');
            $end_date = $end->toDateString();
            SendInvoice::dispatch([
                'user_id' => $user->id,
                'start_date' => $start_date,
                'end_date' => $end_date
            ]);
        }
    }
}
