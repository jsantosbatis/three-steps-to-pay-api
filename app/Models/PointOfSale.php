<?php

namespace App\Models;

use App\Geocoding\Geocoding;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class PointOfSale extends Model
{
    use SoftDeletes;
    use HasFactory;

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($pointOfSale) {
            $maxToken = PointOfSale::max('token');
            $pointOfSale['token'] = $maxToken < 1000 ? 1000 : ++$maxToken;
            $coordinates = Geocoding::getCoordinates($pointOfSale->street . ' ' . $pointOfSale->postal_code . ' ' . $pointOfSale->city);
            if ($coordinates !== null) {
                $pointOfSale->lat = $coordinates->lat;
                $pointOfSale->lng = $coordinates->lng;
            } else {
                $pointOfSale->lat = null;
                $pointOfSale->lng = null;
            }
        });

        static::updating(function ($pointOfSale) {
            $coordinates = Geocoding::getCoordinates($pointOfSale->street . ' ' . $pointOfSale->postal_code . ' ' . $pointOfSale->city);
            if ($coordinates !== null) {
                $pointOfSale->lat = $coordinates->lat;
                $pointOfSale->lng = $coordinates->lng;
            } else {
                $pointOfSale->lat = null;
                $pointOfSale->lng = null;
            }
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'street', 'postal_code', 'city', 'bank_account', 'user_id', 'token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public function products()
    {
        return $this->hasMany('App\Models\Product')
            ->orderBy('product_category_id')
            ->orderBy('name')
            ->orderBy('price');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function getCountProductsAttribute()
    {
        return $this->hasMany('App\Models\Product')->count();
    }

    public function getProductsPerCategory()
    {
        $products = ProductCategory::leftJoin('products', 'products.product_category_id', '=', 'product_categories.id')
            ->where('point_of_sale_id', '=', $this->id)
            ->orderBy('product_category_id')->orderBy('price')->get();

        $result = [];
        $i = 0;
        while ($i < count($products)) {
            $crtCategory = $products[$i]->product_category_id;
            $category = ['id' => $crtCategory, 'name' => ProductCategory::find($crtCategory)->{'name_' . Cache::get('lang')}, 'products' => []];
            while ($i < count($products) && $crtCategory === $products[$i]->product_category_id) {
                array_push($category['products'], $products[$i]);
                $i++;
            }
            array_push($result, $category);
        }

        //Add missing categories without products
        $categories = ProductCategory::all();
        foreach ($categories as $category) {
            $id = $category['id'];
            $findCategory = array_filter($result, function ($item) use ($id) {
                return $item['id'] == $id;
            });
            if (count($findCategory) === 0) {
                array_push($result, ['id' => $id, 'name' => $category['name_' . Cache::get('lang')], 'products' => []]);
            }
        }
        return $result;
    }
}
