<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CallbackHistory extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'payment_id', 'transfer_amount', 'tipping_amount', 'total_amount',
        'description', 'reference', 'created_at', 'expire_at',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
}
