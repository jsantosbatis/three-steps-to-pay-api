<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount', 'net_amount', 'fee', 'date', 'payment_id', 'payment_method_type_id', 'status', 'product_id', 'in_store',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'datetime', // Format date to currrent locale
    ];

    public function transfers()
    {
        return $this->belongsToMany('App\Models\Transfer');
    }

    public function paymentMethodType()
    {
        return $this->hasOne('App\Models\PaymentMethodType');
    }
}
