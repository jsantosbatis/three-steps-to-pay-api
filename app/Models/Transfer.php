<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount', 'communication', 'treated', 'bank_account', 'bank_name', 'invoice_id', 'product_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public function transactions()
    {
        return $this->belongsToMany('App\Models\Transaction');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
