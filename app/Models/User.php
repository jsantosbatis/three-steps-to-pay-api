<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'is_admin', 'use_company_details_for_billing', 'google_id', 'pagi_app_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_admin' => 'boolean',
        'use_company_details_for_billing' => 'boolean',
    ];

    public function company()
    {
        return $this->hasOne('App\Models\Company');
    }

    public function billing_detail()
    {
        return $this->hasOne('App\Models\BillingDetail');
    }

    public static function findUserByEmail($email)
    {
        return User::where('email', '=', $email)->first();
    }
}
