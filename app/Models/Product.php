<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use App\QrCodeApi\QrCodeApi;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
    use SoftDeletes;
    use HasFactory;

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($product) {
            $product['token'] = Str::random(35);
        });

        static::created(function ($product) {
            $product->save(); //Generate payconiq qr code
        });

        static::updating(function ($product) {
            if ($product->qr_code_id) {
                $qrCode = QrCodeApi::updatePayconiqQrCode($product->qr_code_id, [
                    'amount' => intval($product->price),
                    'description' => $product->name,
                    'customer_reference' => $product->token,
                    'product' => [
                        'id' => $product->id,
                        'name' => $product->name,
                        'amount' => intval($product->price),
                        'quantity' => 1,
                    ],
                    'pagi_app_id' => Auth::user()->pagi_app_id,
                ]);
            } else {
                $qrCode = QrCodeApi::generatePayconiqQrCode([
                    'amount' => intval($product->price),
                    'description' => $product->name,
                    'customer_reference' => $product->token,
                    'product' => [
                        'id' => $product->id,
                        'name' => $product->name,
                        'amount' => intval($product->price),
                        'quantity' => 1,
                    ],
                    'pagi_app_id' => Auth::user()->pagi_app_id,
                ]);
            }
            $product->qr_code_id = $qrCode['id'];
            $product->qr_code_url =  $qrCode['qrCodeUrl'];
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'point_of_sale_id', 'qr_code_url', 'qr_code_id', 'token',
        'main_percentage', 'secondary_percentage', 'secondary_bank_account_id', 'product_category_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public function getSecondaryBankAccountAttribute()
    {
        return BankAccount::where('id', '=', $this->attributes['secondary_bank_account_id'])->get();
    }

    public function pointOfSale()
    {
        return $this->belongsTo('App\Models\PointOfSale');
    }
}
