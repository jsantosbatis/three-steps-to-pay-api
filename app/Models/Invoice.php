<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_date', 'end_date', 'amount', 'sales_amount', 'file_path', 'user_id', 'number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public static function getNextNumber()
    {
        $last_value = Invoice::select('number')->orderBy('created_at', 'desc')->first();
        if ($last_value) {
            $next_number = array_reverse(explode('-', $last_value->number))[0] + 1;
            $next_number = Carbon::now()->toDateString() . '-' . $next_number;
        } else {
            $next_number = Carbon::now()->toDateString() . '-1';
        }
        return $next_number;
    }

    public function transfers()
    {
        return $this->hasMany('App\Models\Transfer');
    }
}
