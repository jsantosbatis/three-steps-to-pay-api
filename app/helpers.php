<?php

use App\Models\PaymentRequest;
use Endroid\QrCode\QrCode;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Formatter\IntlMoneyFormatter;
use Money\Money;

if (!function_exists('token_header')) {
    function token_header($token)
    {
        return [
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json'
        ];
    }
}

if (!function_exists('int_to_euro')) {
    function int_to_euro($value, $withEuroChar = true)
    {
        $money = new Money($value, new Currency('EUR'));
        $currencies = new ISOCurrencies();

        $numberFormatter = new \NumberFormatter('fr_BE', $withEuroChar ? \NumberFormatter::DECIMAL_ALWAYS_SHOWN : \NumberFormatter::DECIMAL);
        $moneyFormatter = new IntlMoneyFormatter($numberFormatter, $currencies);

        return  $moneyFormatter->format($money);
    }
}

if (!function_exists('int_to_decimal')) {
    function int_to_decimal($value)
    {
        $money = new Money($value, new Currency('EUR'));
        $currencies = new ISOCurrencies();

        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        return $moneyFormatter->format($money);
    }
}

if (!function_exists('remove_decimal_point')) {
    function remove_decimal_point($value)
    {
        return str_replace('.', '', number_format($value, 2));
    }
}

if (!function_exists('add_churchpay_fee')) {
    function add_churchpay_fee($amount = 0)
    {
        return $amount + remove_decimal_point(config('constants.CHURCHPAY_FEE') + config('constants.CHURCHPAY_FEE') * 0.21);
    }
}

if (!function_exists('translate_payment_method_type')) {
    function translate_payment_method_type($method = 'payconiq')
    {
        switch ($method) {
            case 'payconiq':
                return config('constants.PAYMENT_METHOD_TYPE_ID_PAYCONIQ');
            case 'bancontact':
                return config('constants.PAYMENT_METHOD_TYPE_ID_BANCONTACT');
            case 'card':
                return config('constants.PAYMENT_METHOD_TYPE_ID_CARD');
            case 'paypal':
                return config('constants.PAYMENT_METHOD_TYPE_ID_PAYPAL');
            case 'sofort':
                return config('constants.PAYMENT_METHOD_TYPE_ID_SOFORT');
            default:
                return config('constants.PAYMENT_METHOD_TYPE_ID_PAYCONIQ');;
        }
    }
}

if (!function_exists('create_payment_request')) {
    function create_payment_request($products, $token, $paymentMethodType)
    {
        foreach ($products as $product) {
            PaymentRequest::create([
                'product_id' =>  $product['id'],
                'point_of_sale_id' => $product['point_of_sale_id'],
                'amount' => remove_decimal_point($product['price']),
                'token' => $token,
                'payment_method_type_id' => translate_payment_method_type($paymentMethodType)
            ]);
        }
    }
}

if (!function_exists('generate_qr_code')) {
    function generate_qr_code($url, $pointOfSaleName, $text)
    {
        //Prepare QRCode
        $qrCode = new QrCode($url);
        $qrCode->setSize(450);
        $qrCode->setMargin(10);
        $qrCode->setLogoPath(Storage::disk('local')->path('ic-logo.png'));
        $qrCode->setLogoSize(100, 100);

        //Prepare qrcode frame
        $frame = Storage::get('qrcode-frame.png');
        $height = Image::make($frame)->height();
        $width = Image::make($frame)->width();
        $img = Image::canvas($height, $width);
        $fontPath = Storage::disk('local')->path('fonts/Quicksand-Medium.ttf');

        $img = Image::make($frame)->insert(
            $qrCode->writeDataUri(),
            'top-left',
            395,
            877
        )->text(
            $pointOfSaleName,
            $width / 2,
            1635,
            function ($font) use ($fontPath) {
                $font->file($fontPath);
                $font->size(48);
                $font->color('#4B5563');
                $font->align('center');
            }
        )->text(
            $text,
            $width / 2,
            1445,
            function ($font) use ($fontPath) {
                $font->file($fontPath);
                $font->size(38);
                $font->color('#4B5563');
                $font->align('center');
            }
        );
        return $img->response('png');
    }
}

if (!function_exists('generate_payconiq_frame')) {
    function generate_payconiq_frame($qrCodeUrl, $pointOfSaleName = null, $text = null)
    {
        $payconiqQrCode = Image::make(preg_replace('/s=(S|M|L|XL)/', 's=XL', $qrCodeUrl));
        $payconiqQrCode->resize('450', '450');
        //Church pay frame
        $frame = Storage::get('qrcode-frame-payconiq.png');
        $height = Image::make($frame)->height();
        $width = Image::make($frame)->width();
        $img = Image::canvas($height, $width);
        $fontPath = Storage::path('fonts/Quicksand-Medium.ttf');

        $img = Image::make($frame)->insert(
            $payconiqQrCode,
            'top-left',
            395,
            877
        )->text(
            $pointOfSaleName,
            $width / 2,
            1635,
            function ($font) use ($fontPath) {
                $font->file($fontPath);
                $font->size(48);
                $font->color('#4B5563');
                $font->align('center');
            }
        )->text(
            explode(' ', $text)[0],
            $width - 225,
            1075,
            function ($font) use ($fontPath) {
                $font->file($fontPath);
                $font->size(48);
                $font->color('#4B5563');
                $font->align('center');
            }
        )->text(
            explode(' ', $text)[1],
            $width - 225,
            1135,
            function ($font) use ($fontPath) {
                $font->file($fontPath);
                $font->size(48);
                $font->color('#4B5563');
                $font->align('center');
            }
        );
        return $img->response('png');
    }
}

if (!function_exists('map_transaction_sales')) {
    function map_transaction_sales($collection)
    {
        $collection = $collection->groupBy('payment_id');
        $data = [];
        foreach ($collection as $item) {
            $row = [];
            if (count($item) === 1) {
                $row['point_of_sale_name'] = $item[0]['point_of_sale_name'];
                $row['payment_method_type_name'] = $item[0]['payment_method_type_name'];
                $row['payment_method_type_id'] = $item[0]['payment_method_type_id'];
                $row['product_name'] = $item[0]['product_name'];
                $row['date'] = $item[0]['date'];
                $row['fee'] = $item[0]['fee'];
                $row['net_amount'] = $item[0]['net_amount'];
                $row['amount'] = $item[0]['amount'];
                $row['product_count'] = 0;
                $row['created_at'] =  $item[0]['created_at'];
                array_push($data, $row);
            } else {
                for ($i = 0; $i < count($item); $i++) {
                    $transaction = $item[$i];
                    if ($transaction['in_store'] === 1) {
                        $row['point_of_sale_name'] = $transaction['point_of_sale_name'];
                        $row['payment_method_type_name'] = $transaction['payment_method_type_name'];
                        $row['payment_method_type_id'] = $transaction['payment_method_type_id'];
                        $row['product_name'] = $transaction['product_name'];
                        $row['date'] = $transaction['date'];
                        $row['fee'] = $transaction['fee'];
                        $row['net_amount'] = $transaction['net_amount'];
                        $row['amount'] = $transaction['amount'];
                        $row['product_count'] = 0;
                        $row['created_at'] = $transaction['created_at'];
                        array_push($data, $row);
                    } else {
                        if ($i === 0) {
                            $row['point_of_sale_name'] = $transaction['point_of_sale_name'];
                            $row['payment_method_type_name'] = $transaction['payment_method_type_name'];
                            $row['payment_method_type_id'] = $transaction['payment_method_type_id'];
                            $row['date'] = $transaction['date'];
                            $row['fee'] = 0;
                            $row['net_amount'] = 0;
                            $row['amount'] = 0;
                            $row['created_at'] = $transaction['created_at'];
                            $row['product_count'] = count($item);
                        }
                        $row['fee'] += $transaction['fee'];
                        $row['net_amount'] += $transaction['net_amount'];
                        $row['amount'] += $transaction['amount'];
                    }
                }
            }
            if ($row['product_count'] > 0) {
                array_push($data, $row);
            }
        }
        return $data;
    }
}



if (!function_exists('map_items')) {
    function map_items($items)
    {
        return collect($items)->reduce(function ($output, $product) {
            $product_id = $product['id'];
            $result = array_filter($output['items'], function ($item) use ($product_id) {
                return $item['id'] == $product_id;
            });
            if (count($result) === 0) {
                $copy = [
                    'id' => $product['id'],
                    'name' => $product['name'],
                    'amount' => is_float($product['amount']) ? intval(remove_decimal_point(floatval($product['amount']))) : intval($product['amount']),
                    "quantity" => 1
                ];
                $output['total'] += intval($copy['amount']);
                array_push($output['items'], $copy);
            } else {
                $output['items'][array_search(array_pop($result), $output['items'])]['quantity']++;
                $output['total'] += intval($product['amount']);
            }

            return $output;
        }, array('total' => 0, 'items' => []));
    }
}
