<?php

namespace App\QrCodeApi;

use Illuminate\Support\Facades\Http;

class QrCodeApi
{
    public static function generatePayconiqQrCode($params)
    {
        $response = Http::post(env('QR_CODE_API_GRAPHQL'), [
            'query' => '
                mutation createPayconiqQRCode {
                    createPayconiqQRCode(
                    appId:"' . $params['pagi_app_id'] . '",
                    transactionInput: {
                        amount:  ' . intval($params['amount']) . ',
                        description: "' . $params['description'] . '",
                        customerReference: "' . $params['customer_reference'] . '",
                        product:' .  preg_replace('/"([^"]+)"\s*:\s*/', '$1:', json_encode($params['product'])) . '
                    }
                ) {
                    id
                    qrCodeUrl
                }
            }'
        ]);
        return $response->json('data.createPayconiqQRCode');
    }

    public static function updatePayconiqQrCode($id, $params)
    {
        $response = Http::post(env('QR_CODE_API_GRAPHQL'), [
            'query' => '
                mutation updatePayconiqQRCode {
                    updatePayconiqQRCode(
                    id:"' . $id . '",
                    transactionInput: {
                        amount:  ' . intval($params['amount']) . ',
                        description: "' . $params['description'] . '",
                        customerReference: "' . $params['customer_reference'] . '",
                        product:' .  preg_replace('/"([^"]+)"\s*:\s*/', '$1:', json_encode($params['product'])) . '
                    }
                ) {
                    id
                    qrCodeUrl
                }
            }'
        ]);

        return $response->json('data.updatePayconiqQRCode');
    }

    public static function generatePayconiqFrame($qrCodeUrl)
    {
        $response = Http::post(
            env('QR_CODE_API_URL') . '/payconiq/frame',
            [
                'url' => $qrCodeUrl
            ]
        );
        return $response;
    }

    public static function checkout($params)
    {
        $products = $params['products'];
        $lang = $params['lang'] ?? 'en';
        $response = Http::post(env('QR_CODE_API_GRAPHQL'), [
            'query' => '
                mutation checkout {
                checkout(
                    appId:"' . $params['pagi_app_id'] . '"
                    lang: "' . $lang . '"
                    transactionInput: {
                        amount:  ' . intval($products['total']) . ',
                        description: "ChurchPay",
                        customerReference: "' . $params['token'] . '",
                        products:' .  preg_replace('/"([^"]+)"\s*:\s*/', '$1:', json_encode($products['items'])) . '
                    }
                ) {
                    url
                }
            }'
        ]);
        return $response->json('data.checkout');
    }
}
