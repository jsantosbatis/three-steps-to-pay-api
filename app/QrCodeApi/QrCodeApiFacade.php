<?php

namespace App\QrCodeApi;

use Illuminate\Support\Facades\Facade;

class QrCodeApiFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'qrcode_api';
    }
}
