<?php

namespace App\Policies;

use App\Models\PointOfSale;
use App\Models\Product;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function isOwner(User $user, Product $product)
    {
        $pointOfSale = PointOfSale::findOrFail($product->point_of_sale_id);
        return ($user->id == $pointOfSale->user_id) || ($user->is_admin);
    }
}
