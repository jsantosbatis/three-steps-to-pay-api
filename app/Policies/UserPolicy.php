<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function isAdmin(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Admin user can update any users.
     * Normal user can only update them own profil.
     */
    public function isHimSelf(User $user)
    {
        $authUser = Auth::user();
        return ($authUser->id == $user->id) || ($authUser->is_admin);
    }
}
