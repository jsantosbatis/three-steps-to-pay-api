<?php

namespace App\Policies;

use App\Models\PointOfSale;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class PointOfSalePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function isOwner(User $user, PointOfSale $pointOfSale)
    {
        return ($user->id == $pointOfSale->user_id) || ($user->is_admin);
    }
}
