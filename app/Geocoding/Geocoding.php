<?php

namespace App\Geocoding;

use Illuminate\Support\Facades\Http;

class Geocoding
{
    const apiUrl = 'https://maps.googleapis.com/maps/api/geocode/json';
    public static function getCoordinates($address)
    {
        $response = Http::get(Geocoding::apiUrl, ['address' => $address, 'key' => env('GEOCODING_API_KEY')]);
        $response = json_decode($response->body());
        if ($response->status === 'OK') {
            return $response->results[0]->geometry->location;
        } else {
            return null;
        }
    }
}
