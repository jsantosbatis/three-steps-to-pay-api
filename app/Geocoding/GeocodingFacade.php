<?php

namespace App\Geocoding;

use Illuminate\Support\Facades\Facade;

class GeocodingFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'geocoding';
    }
}
