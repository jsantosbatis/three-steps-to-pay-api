<?php

use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConvertDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $transactions = Transaction::all();

        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('date');
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->timestamp('date')->after('amount')->required()->nullable();
        });

        foreach ($transactions as $transaction) {
            try {
                $transaction->date = Carbon::parse($transaction->date);
            } catch (Exception $e) {
                $transaction->date = null;
            }
            $transaction->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
