<?php

namespace Database\Factories;

use App\Models\PointOfSale;
use Illuminate\Database\Eloquent\Factories\Factory;

class PointOfSaleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PointOfSale::class;

    public function configure()
    {
        return $this->afterMaking(function (PointOfSale $pointOfSale) {
            //
        })->afterCreating(function (PointOfSale $pointOfSale) {
            //
        });
    }

    public function definition()
    {
        return [
            'name' => $this->faker->company(),
            'street' => $this->faker->streetAddress(),
            'postal_code' => $this->faker->postcode(),
            'city' => $this->faker->city(),
            'bank_account' => $this->faker->bankAccountNumber()
        ];
    }

    public function missing_name()
    {
        return $this->state([
            'name' => null
        ]);
    }

    public function church_1()
    {
        return $this->state([
            'name' => 'Notre Dame Immaculée',
            'street' => 'Place du Jeu de balle',
            'postal_code' => '1000',
            'city' => 'Bruxelles',
            'bank_account' => $this->faker->bankAccountNumber()
        ]);
    }

    public function church_2()
    {
        return $this->state([
            'name' => 'Notre Dame du Sablon',
            'street' =>  'Rue des Sablons',
            'postal_code' => '1000',
            'city' => 'Bruxelles',
            'bank_account' => $this->faker->bankAccountNumber()
        ]);
    }

    public function church_3()
    {
        return $this->state([
            'name' => 'Saint-Christophe',
            'street' =>  'Place Charles II',
            'postal_code' => '6000',
            'city' => 'Charleroi',
            'bank_account' => $this->faker->bankAccountNumber()
        ]);
    }
}
