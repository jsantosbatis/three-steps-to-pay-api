<?php

namespace Database\Factories;

use App\Models\BankAccount;
use Illuminate\Database\Eloquent\Factories\Factory;

class BankAccountFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BankAccount::class;

    public function configure()
    {
        return $this->afterMaking(function (BankAccount $bankAccount) {
            //
        })->afterCreating(function (BankAccount $bankAccount) {
            //
        });
    }

    public function definition()
    {
        return [
            'name' => $this->faker->company(),
            'number' =>  $this->faker->bankAccountNumber(),
        ];
    }

    public function missing_name()
    {
        return $this->state([
            'name' => null,
        ]);
    }

    public function missing_number()
    {
        return $this->state([
            'number' => null,
        ]);
    }
}
