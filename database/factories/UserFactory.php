<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    public function configure()
    {
        return $this->afterMaking(function (User $product) {
            //
        })->afterCreating(function (User $product) {
            //
        });
    }

    public function definition()
    {
        return [
            'email' => $this->faker->unique()->safeEmail,
            'is_admin' => false,
            'use_company_details_for_billing' => true
        ];
    }

    public function admin()
    {
        return $this->state([
            'is_admin' => true,
        ]);
    }

    public function with_billing_detail()
    {
        return $this->state([
            'use_company_details_for_billing' => false,
        ]);
    }

    public function missing_email()
    {
        return $this->state([
            'email' => null,
        ]);
    }
}
