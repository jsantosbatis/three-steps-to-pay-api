<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    public function configure()
    {
        return $this->afterMaking(function (Product $product) {
            //
        })->afterCreating(function (Product $product) {
            //
        });
    }

    public function definition()
    {
        return $this->faker->numberBetween(0, 1) == 1 ?
            [
                'name' => $this->faker->word(),
                'price' => $this->faker->numberBetween(100, 900),
                'main_percentage' => 100,
                'secondary_percentage' => 0,
                'product_category_id' => 1
            ] :
            [
                'name' => $this->faker->word(),
                'price' => $this->faker->numberBetween(100, 900),
                'main_percentage' => 100,
                'secondary_percentage' => 0,
                'product_category_id' => 1
            ];
    }

    public function candle_1()
    {
        return $this->state([
            'name' => 'Bougie',
            'price' => 100,
            'main_percentage' => 100,
            'secondary_percentage' => 0,
            'product_category_id' => 1
        ]);
    }

    public function candle_9()
    {
        return $this->state([
            'name' => 'Neuvaine',
            'price' => 900,
            'main_percentage' => 100,
            'secondary_percentage' => 0,
            'product_category_id' => 1
        ]);
    }

    public function gift_5()
    {
        return $this->state([
            'name' => 'Don',
            'price' => 500,
            'main_percentage' => 100,
            'secondary_percentage' => 0,
            'product_category_id' => 1
        ]);
    }

    public function donation_5()
    {
        return $this->state([
            'name' => 'Collecte',
            'price' => 500,
            'main_percentage' => 100,
            'secondary_percentage' => 0,
            'product_category_id' => 1
        ]);
    }
}
