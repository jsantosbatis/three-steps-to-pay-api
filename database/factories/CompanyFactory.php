<?php

namespace Database\Factories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    public function configure()
    {
        return $this->afterMaking(function (Company $company) {
            //
        })->afterCreating(function (Company $company) {
            //
        });
    }

    public function definition()
    {
        return [
            'name' => $this->faker->company(),
            'street' => $this->faker->streetAddress(),
            'postal_code' => $this->faker->postcode(),
            'city' => $this->faker->city(),
            'vat_number' => $this->faker->vat()
        ];
    }

    public function missing_name()
    {
        return $this->state([
            'name' => null
        ]);
    }

    public function missing_street()
    {
        return $this->state([
            'street' => null
        ]);
    }

    public function missing_postal_code()
    {
        return $this->state([
            'postal_code' => null
        ]);
    }

    public function missing_city()
    {
        return $this->state([
            'city' => null
        ]);
    }

    public function missing_vat_number()
    {
        return $this->state([
            'vat_number' => null
        ]);
    }
}
