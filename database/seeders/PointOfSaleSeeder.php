<?php

namespace Database\Seeders;

use App\Models\PointOfSale;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Testing\WithFaker;

class PointOfSaleSeeder extends Seeder
{
    use WithFaker;

    public function __construct()
    {
        $this->setUpFaker();
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('APP_ENV') != 'production') {
            //CHURCH 1
            $church1 = PointOfSale::factory()->church_1()->make();
            $church1->user_id = User::where('email', '=', 'joaopaulo0503@gmail.com')->first()->id;
            $church1 = PointOfSale::create($church1->toArray());

            //CHURCH 1 - Products
            $product = Product::factory()->candle_1()->make();
            $product->point_of_sale_id = $church1->id;
            $product = Product::create($product->toArray());

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' => $this->faker->randomNumber(),
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1
            ]);

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' => $this->faker->randomNumber(),
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1
            ]);
            $paymentId = $this->faker->randomNumber();
            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.online_payment_fees')),
                'fee' => config('payconiq.online_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' =>  $paymentId,
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1,
                'in_store' => false
            ]);

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.online_payment_fees')),
                'fee' => config('payconiq.online_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' =>  $paymentId,
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1,
                'in_store' => false
            ]);
            //CHURCH 1 - Products
            $product = Product::factory()->candle_9()->make();
            $product->point_of_sale_id = $church1->id;
            $product = Product::create($product->toArray());

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' => $this->faker->randomNumber(),
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1
            ]);

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' => $this->faker->randomNumber(),
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1
            ]);

            $paymentId = $this->faker->randomNumber();
            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' =>  $paymentId,
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1,
                'in_store' => false
            ]);

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' =>  $paymentId,
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1,
                'in_store' => false
            ]);

            //CHURCH 1 - Products
            $product = Product::factory()->gift_5()->make();
            $product->point_of_sale_id = $church1->id;
            $product = Product::create($product->toArray());

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' => $this->faker->randomNumber(),
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1
            ]);

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' => $this->faker->randomNumber(),
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1
            ]);

            //CHURCH 1 - Products
            $product = Product::factory()->donation_5()->make();
            $product->point_of_sale_id = $church1->id;
            $product = Product::create($product->toArray());

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' => $this->faker->randomNumber(),
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1
            ]);

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' => $this->faker->randomNumber(),
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1
            ]);


            //CHURCH 2
            $church2 = PointOfSale::factory()->church_2()->make();
            $church2->user_id = User::where('email', '=', 'joaopaulo0503@gmail.com')->first()->id;
            $church2 = PointOfSale::create($church2->toArray());

            //CHURCH 2 - Products
            $product = Product::factory()->candle_9()->make();
            $product->point_of_sale_id = $church2->id;
            $product = Product::create($product->toArray());

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' => $this->faker->randomNumber(),
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1
            ]);

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' => $this->faker->randomNumber(),
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1
            ]);

            //CHURCH 2 - Products
            $product = Product::factory()->gift_5()->make();
            $product->point_of_sale_id = $church2->id;
            $product = Product::create($product->toArray());

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' => $this->faker->randomNumber(),
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1
            ]);

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' => $this->faker->randomNumber(),
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1
            ]);

            //CHURCH 2 - Products
            $product = Product::factory()->donation_5()->make();
            $product->point_of_sale_id = $church2->id;
            $product = Product::create($product->toArray());

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' => $this->faker->randomNumber(),
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1
            ]);

            Transaction::create([
                'amount' => $product->price,
                'net_amount' => $product->price - add_churchpay_fee(config('payconiq.instore_payment_fees')),
                'fee' => config('payconiq.instore_payment_fees'),
                'date' => Carbon::now(),
                'payment_id' => $this->faker->randomNumber(),
                'status' => 'SUCCEEDED',
                'product_id' => $product->id,
                'payment_method_type_id' => 1
            ]);
        }
    }
}
