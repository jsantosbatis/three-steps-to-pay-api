<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::factory()->admin()->make();
        $user->email = 'joaopaulo0503@gmail.com';
        if (env('APP_ENV') == 'production') {
            $user->google_id = 'RdkI3GeTeMhU8jDG7DMLppy32lc2';
            $user->pagi_app_id = 'a933472e-e06a-4adc-8843-84ec1556d680';
        } else {
            $user->google_id = '6VKnBoRbl7aix8j7MrbI5Yjr8R73';
            $user->pagi_app_id = '24f705e5-a87f-4d6c-99c0-0d312e1c96e0';
        }
        $user = User::create($user->toArray());
        $company = Company::factory()->make();
        $company->user_id = $user->id;
        Company::create($company->toArray());
    }
}
