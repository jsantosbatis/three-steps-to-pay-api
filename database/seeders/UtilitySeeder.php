<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UtilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_categories')->insert([
            [
                'name_fr' => 'Bougie',
                'name_en' => 'Candle',
                'name_nl' => 'Kaars'
            ],
            [
                'name_fr' => 'Don',
                'name_en' => 'Donation',
                'name_nl' => 'Schenking'
            ],
            [
                'name_fr' => 'Collecte',
                'name_en' => 'Collection',
                'name_nl' => 'Collecte'
            ],
            [
                'name_fr' => 'Article',
                'name_en' => 'Item',
                'name_nl' => 'Artikel'
            ],
            [
                'name_fr' => 'Ticket',
                'name_en' => 'Ticket',
                'name_nl' => 'Ticket'
            ],
        ]);

        DB::table('payment_method_types')->insert([
            [
                'name' => 'Payconiq',
            ],
            [
                'name' => 'Card',
            ],
            [
                'name' => 'Bancontact'
            ],
            [
                'name' => 'Paypal'
            ],
            [
                'name' => 'Sepa'
            ],
            [
                'name' => 'Sofort'
            ],
        ]);
    }
}
