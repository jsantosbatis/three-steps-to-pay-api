<?php

namespace Tests\Feature;

use App\Models\PointOfSale;
use App\Models\Product;
use App\Models\User;
use App\QrCodeApi\QrCodeApi;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->auth = app('firebase.auth');
        $admin = User::factory()->admin()->make();
        $admin->google_id = $this->auth->getUserByEmail('admin@gmail.com')->uid;
        $this->admin = User::create($admin->toArray());
        $this->token =  $admin->google_id;
    }

    /**
     * Create product
     *
     * @return void
     */
    public function testCreateProduct()
    {
        $product = $this->_createProduct();
        $response = $this->post('api/products', $product->toArray(), token_header($this->token));
        $response->assertStatus(201)
            ->assertJsonStructure([
                'id'
            ]);
    }

    /**
     * Create product with invalid data (price not provided)
     *
     * @return void
     */
    public function testCreateProductWithInvalidData()
    {
        $product = $this->_createProduct();
        $product = $product->toArray();
        $product['price'] = null;
        $response = $this->post('api/products', $product, token_header($this->token));
        $response->assertStatus(400);
    }

    /**
     * Edit product
     *
     * @return void
     */
    public function testEditProduct()
    {
        $product = Product::create($this->_createProduct()->toArray());
        $product->name = 'Une bougie';
        $product->product_category_id = config('constants.PRODUCT_GROUP_ID_COLLECTION');
        $response = $this->patch('api/products/' . $product->id, $product->toArray(), token_header($this->token));
        $response->assertStatus(200)
            ->assertJsonFragment([
                'name' => 'Une bougie',
                'product_category_id' => config('constants.PRODUCT_GROUP_ID_COLLECTION'),
            ]);
    }


    /**
     * Edit product that affect the qr code
     *
     * @return void
     */
    public function testEditProductWithEffectOnQrCode()
    {
        $product = Product::create($this->_createProduct()->toArray());
        $product->name = 'Une bougie';
        $product->price = 10.00;
        $response = $this->patch('api/products/' . $product->id,  $product->toArray(), token_header($this->token));
        $response->assertStatus(200)
            ->assertJsonFragment([
                'name' => 'Une bougie',
                'price' => "10.00",
                'qr_code_url' => 'something'
            ]);
    }

    /**
     * Edit product with invalid data (price not provided)
     *
     * @return void
     */
    public function testEditProductWithInvalidData()
    {

        $product = Product::create($this->_createProduct()->toArray());
        $product->price = null;
        $response = $this->patch('api/products/' . $product->id, $product->toArray(), token_header($this->token));
        $response->assertStatus(400);
    }

    /**
     * Edit product that not exist
     *
     * @return void
     */
    public function testEditProductThatNotExist()
    {
        $product = $this->_createProduct();
        $product->id = 5;
        $response = $this->patch('api/products/' . $product->id, $product->toArray(), token_header($this->token));
        $response->assertStatus(404);
    }

    /**
     * Delete product
     *
     * @return void
     */
    public function testDeleteProduct()
    {
        $product = Product::create($this->_createProduct()->toArray());
        $response = $this->delete('api/products/' . $product->id, [], token_header($this->token));
        $response->assertStatus(200);
        $this->assertTrue(
            Product::withTrashed()
                ->where('id', $product->id)->first()->trashed()
        );
    }

    /**
     * Delete product that not exist
     *
     * @return void
     */
    public function testDeleteProductThatNotExist()
    {
        $response = $this->delete('api/products/5', [], token_header($this->token));
        $response
            ->assertStatus(404);
    }

    public function _createProduct()
    {
        $user = User::factory()->create();
        $pointOfSale = PointOfSale::factory()->make();
        $pointOfSale->user_id = $user->id;
        $pointOfSale = PointOfSale::create($pointOfSale->toArray());
        $product = Product::factory()->make();
        $product->point_of_sale_id = $pointOfSale->id;
        return $product;
    }
}
