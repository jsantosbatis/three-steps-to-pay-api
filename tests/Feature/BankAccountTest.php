<?php

namespace Tests\Feature;

use App\Models\BankAccount;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BankAccountTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->auth = app('firebase.auth');
        $admin = User::factory()->admin()->make();
        $admin->google_id = $this->auth->getUserByEmail('admin@gmail.com')->uid;
        $this->admin = User::create($admin->toArray());
        $this->token =  $admin->google_id;
    }

    /**
     * Create bank account
     *
     * @return void
     */
    public function testCreateBankAccount()
    {
        $bankAccount = BankAccount::factory()->make();
        $response = $this->postJson(
            'api/bank_accounts',
            $bankAccount->toArray(),
            token_header($this->token)
        );
        $response->assertStatus(201)
            ->assertJsonStructure([
                'id'
            ]);
    }

    /**
     * Create invalid bank account (with missing name)
     *
     * @return void
     */
    public function testCreateBankAccountWithInvalidName()
    {
        $bankAccount = BankAccount::factory()->missing_name()->make();
        $response = $this->post(
            'api/bank_accounts',
            $bankAccount->toArray(),
            token_header($this->token)
        );
        $response->assertStatus(400);
    }


    /**
     * Create invalid bank account (with missing number)
     *
     * @return void
     */
    public function testCreateBankAccountWithInvalidNumber()
    {
        $bankAccount = BankAccount::factory()->missing_number()->make();
        $response = $this->post(
            'api/bank_accounts',
            $bankAccount->toArray(),
            token_header($this->token)
        );
        $response->assertStatus(400);
    }

    /**
     * Edit bank account
     *
     * @return void
     */
    public function testEditBankAccountName()
    {
        $bankAccount = BankAccount::factory()->make();
        $response = $this->post(
            'api/bank_accounts',
            $bankAccount->toArray(),
            token_header($this->token)
        );
        $bankAccount->id = json_decode($response->getContent())->id;
        $bankAccount->name = 'Notre Dame Immaculée';
        $response = $this->patch(
            'api/bank_accounts/' . $bankAccount->id,
            $bankAccount->toArray(),
            token_header($this->token)
        );
        $response->assertStatus(200)
            ->assertJsonFragment([
                'name' => 'Notre Dame Immaculée'
            ]);
    }

    /**
     * Edit bank account
     *
     * @return void
     */
    public function testEditBankAccountNumber()
    {
        $bankAccount = BankAccount::factory()->make();
        $response = $this->post(
            'api/bank_accounts',
            $bankAccount->toArray(),
            token_header($this->token)
        );
        $bankAccount->id = json_decode($response->getContent())->id;
        $bankAccount->number = 'BE98898998989898';
        $response = $this->patch(
            'api/bank_accounts/' . $bankAccount->id,
            $bankAccount->toArray(),
            token_header($this->token)
        );
        $response->assertStatus(200)
            ->assertJsonFragment([
                'number' => 'BE98898998989898'
            ]);
    }


    /**
     * Edit bank account with invalid data
     *
     * @return void
     */
    public function testEditBankAccountWithInvalidNumber()
    {
        $bankAccount = BankAccount::factory()->make();
        $response = $this->post(
            'api/bank_accounts',
            $bankAccount->toArray(),
            token_header($this->token)
        );
        $bankAccount->id = json_decode($response->getContent())->id;
        $bankAccount->number = null;
        $response = $this->patch(
            'api/bank_accounts/' . $bankAccount->id,
            $bankAccount->toArray(),
            token_header($this->token)
        );
        $response->assertStatus(400);
    }


    /**
     * Edit bank account with invalid data
     *
     * @return void
     */
    public function testEditBankAccountWithInvalidName()
    {
        $bankAccount = BankAccount::factory()->make();
        $response = $this->post(
            'api/bank_accounts',
            $bankAccount->toArray(),
            token_header($this->token)
        );
        $bankAccount->id = json_decode($response->getContent())->id;
        $bankAccount->name = null;
        $response = $this->patch(
            'api/bank_accounts/' . $bankAccount->id,
            $bankAccount->toArray(),
            token_header($this->token)
        );
        $response->assertStatus(400);
    }


    /**
     * Delete bank account
     *
     * @return void
     */
    public function testDeleteBankAccount()
    {
        $bankAccount = BankAccount::factory()->make();
        $response = $this->post(
            'api/bank_accounts',
            $bankAccount->toArray(),
            token_header($this->token)
        );
        $bankAccount->id = json_decode($response->getContent())->id;
        $response = $this->delete(
            'api/bank_accounts/' . $bankAccount->id,
            [],
            token_header($this->token)
        );
        $response->assertStatus(200);
        $this->assertNull(
            BankAccount::where('id', '=', $bankAccount->id)->first()
        );
    }
}
