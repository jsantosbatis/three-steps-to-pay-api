<?php

namespace Tests\Feature;

use App\Models\PointOfSale;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PointOfSaleTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->auth = app('firebase.auth');
        $admin = User::factory()->admin()->make();
        $admin->google_id = $this->auth->getUserByEmail('admin@gmail.com')->uid;
        $this->admin = User::create($admin->toArray());
        $this->token =  $admin->google_id;
    }

    /**
     * Create point of sale
     *
     * @return void
     */
    public function testCreatePointOfSale()
    {
        $pointOfSale = PointOfSale::factory()->make();
        $response = $this->post(
            'api/point_of_sales',
            $pointOfSale->toArray(),
            token_header($this->token)
        );
        $response->assertStatus(201)
            ->assertJsonStructure([
                'id'
            ]);
    }

    /**
     * Create point of sale with invalid data (User id not provided)
     *
     * @return void
     */
    public function testCreatePointOfSaleWithInvalidData()
    {
        $pointOfSale = PointOfSale::factory()->missing_name()->make();
        $response = $this->post(
            'api/point_of_sales',
            $pointOfSale->toArray(),
            token_header($this->token)
        );
        $response->assertStatus(400);
    }

    /**
     * Edit point of sale
     *
     * @return void
     */
    public function testEditPointOfSale()
    {
        $pointOfSale = PointOfSale::factory()->make();
        $response = $this->post(
            'api/point_of_sales',
            $pointOfSale->toArray(),
            token_header($this->token)
        );
        $pointOfSale->id = json_decode($response->getContent())->id;
        $pointOfSale->name = 'Notre Dame Immaculée';
        $response = $this->patch(
            'api/point_of_sales/' . $pointOfSale->id,
            $pointOfSale->toArray(),
            token_header($this->token)
        );
        $response->assertStatus(200)
            ->assertJsonFragment([
                'name' => 'Notre Dame Immaculée'
            ]);
    }

    /**
     * Edit point of sale with invalid data (User id not provided)
     *
     * @return void
     */
    public function testEditPointOfSaleWithInvalidData()
    {
        $pointOfSale = PointOfSale::factory()->make();
        $response = $this->post(
            'api/point_of_sales',
            $pointOfSale->toArray(),
            token_header($this->token)
        );
        $pointOfSale->id = json_decode($response->getContent())->id;
        $pointOfSale->name = null;
        $response = $this->patch(
            'api/point_of_sales/' . $pointOfSale->id,
            $pointOfSale->toArray(),
            token_header($this->token)
        );
        $response->assertStatus(400);
    }


    /**
     * Edit point of sale that not exist
     *
     * @return void
     */
    public function testEditPointOfSaleThatNotExist()
    {
        $pointOfSale = PointOfSale::factory()->make();
        $pointOfSale->id = 5;
        $response = $this->patch(
            'api/point_of_sales/' . $pointOfSale->id,
            $pointOfSale->toArray(),
            token_header($this->token)
        );
        $response->assertStatus(404);
    }

    /**
     * Delete point of sale
     *
     * @return void
     */
    public function testDeletePointOfSale()
    {
        $pointOfSale = PointOfSale::factory()->make();
        $response = $this->post(
            'api/point_of_sales',
            $pointOfSale->toArray(),
            token_header($this->token)
        );
        $pointOfSale->id = json_decode($response->getContent())->id;
        for ($i = 0; $i < 5; $i++) {
            $product = Product::factory()->make();
            $product->point_of_sale_id = $pointOfSale->id;
            Product::create($product->toArray());
        }
        $response = $this->delete(
            'api/point_of_sales/' . $pointOfSale->id,
            [],
            token_header($this->token)
        );
        $response
            ->assertStatus(200);
        $this->assertTrue(
            PointOfSale::withTrashed()->where('id', '=', $pointOfSale->id)->first()->trashed()
        );
    }

    /**
     * Delete point of sale with products
     *
     * @return void
     */
    public function testDeletePointOfSaleWithProducts()
    {
        $pointOfSale = PointOfSale::factory()->make();
        $response = $this->post(
            'api/point_of_sales',
            $pointOfSale->toArray(),
            token_header($this->token)
        );


        $pointOfSale->id = json_decode($response->getContent())->id;
        $response = $this->delete(
            'api/point_of_sales/' . $pointOfSale->id,
            [],
            token_header($this->token)
        );
        $response
            ->assertStatus(200);
        $this->assertNull(
            PointOfSale::where('id', $pointOfSale->id)->first()
        );
    }

    /**
     * Delete point of sale that not exist
     *
     * @return void
     */
    public function testDeletePointOfSaleThatNotExist()
    {
        $response = $this->delete('api/point_of_sales/5', [], token_header($this->token));
        $response
            ->assertStatus(404);
    }

    /**
     * Get point of sale
     *
     * @return void
     */
    public function testGetPointOfSale()
    {
        $pointOfSale = PointOfSale::factory()->make();
        $response = $this->post(
            'api/point_of_sales',
            $pointOfSale->toArray(),
            token_header($this->token)
        );
        $pointOfSale->id = json_decode($response->getContent())->id;
        $response = $this->get('api/point_of_sales/' . $pointOfSale->id, token_header($this->token));
        $response
            ->assertStatus(200)
            ->assertJson($pointOfSale->toArray());
    }


    /**
     * Get point of sale that not exist
     *
     * @return void
     */
    public function testGetPointOfSaleThatNotExist()
    {
        $response = $this->get('api/point_of_sales/5', token_header($this->token));
        $response
            ->assertStatus(404);
    }

    /**
     * Get point of sales
     *
     * @return void
     */
    public function testGetPointOfSales()
    {
        for ($i = 0; $i < 5; $i++) {
            $pointOfSale = PointOfSale::factory()->make();
            $pointOfSale->user_id = $this->admin->id;
            $this->post(
                'api/point_of_sales',
                $pointOfSale->toArray(),
                token_header($this->token)
            );
        }
        $response = $this->get('api/point_of_sales', token_header($this->token));
        $response
            ->assertStatus(200)
            ->assertJsonCount(5);
    }

    /**
     * Get products of a point of sale
     *
     * @return void
     */
    public function testGetPointOfSaleProducts()
    {
        $pointOfSale = PointOfSale::factory()->make();
        $pointOfSale->user_id = $this->admin->id;
        $response = $this->post(
            'api/point_of_sales',
            $pointOfSale->toArray(),
            token_header($this->token)
        );
        $pointOfSale->id = json_decode($response->getContent())->id;
        for ($i = 0; $i < 5; $i++) {
            $product = Product::factory()->make();
            $product->point_of_sale_id = $pointOfSale->id;
            Product::create($product->toArray());
        }
        $response = $this->get('api/point_of_sales/' . $pointOfSale->id . '/products', token_header($this->token));
        $response
            ->assertStatus(200)
            ->assertJsonCount(5);
    }

    /**
     * Get products of an empty point of sale
     *
     * @return void
     */
    public function testGetPointOfSaleProductsZero()
    {
        $pointOfSale = PointOfSale::factory()->make();
        $pointOfSale->user_id = $this->admin->id;
        $response = $this->post(
            'api/point_of_sales',
            $pointOfSale->toArray(),
            token_header($this->token)
        );
        $pointOfSale->id = json_decode($response->getContent())->id;
        $response = $this->get('api/point_of_sales/' . $pointOfSale->id . '/products', token_header($this->token));
        $response
            ->assertStatus(200)
            ->assertJsonCount(0);
    }

    public function testFindPointOfSalesInBrussels_app()
    {

        $response = $this->post(
            'api/point_of_sales',
            PointOfSale::factory()->church_1()->make()->toArray(),
            token_header($this->token)
        );
        $response = $this->post(
            'api/point_of_sales',
            PointOfSale::factory()->church_2()->make()->toArray(),
            token_header($this->token)
        );
        $response = $this->post(
            'api/point_of_sales',
            PointOfSale::factory()->church_3()->make()->toArray(),
            token_header($this->token)
        );

        $response = $this->get(
            'api/app/point_of_sales/find_geo?lat=50.835728&lng=4.349840',
            token_header($this->token)
        );

        $response
            ->assertStatus(200)
            ->assertJsonCount(2);
    }

    public function testFindPointOfSalesNotInBrussels_app()
    {

        $response = $this->post(
            'api/point_of_sales',
            PointOfSale::factory()->church_1()->make()->toArray(),
            token_header($this->token)
        );
        $response = $this->post(
            'api/point_of_sales',
            PointOfSale::factory()->church_2()->make()->toArray(),
            token_header($this->token)
        );
        $response = $this->post(
            'api/point_of_sales',
            PointOfSale::factory()->church_3()->make()->toArray(),
            token_header($this->token)
        );

        $response = $this->get(
            'api/app/point_of_sales/find_geo?lat=50.425862&lng=4.465299',
            token_header($this->token)
        );

        $response
            ->assertStatus(200)
            ->assertJsonCount(1);
    }
}
