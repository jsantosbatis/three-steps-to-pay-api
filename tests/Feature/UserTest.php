<?php

namespace Tests\Feature;

use App\Models\BillingDetail;
use App\Models\Company;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->auth = app('firebase.auth');
        $admin = User::factory()->admin()->make();
        $admin->google_id = $this->auth->getUserByEmail('admin@gmail.com')->uid;
        $this->admin = User::create($admin->toArray());
        $this->token =  $admin->google_id;
    }

    /**
     * Create google user
     * 
     * @return void
     */
    public function testCreateGoogleUser()
    {
        $user = User::factory()->make();
        $uid = $this->auth->createUser(['email' => $user->email, 'password' => 'mypassword'])->uid;
        $user = $this->auth->getUser($uid);
        $this->assertNotNull($user);
        $this->auth->deleteUser($uid);
    }

    /**
     * Create an user
     *
     * @return void
     */
    public function testCreateUser()
    {
        $user = User::factory()->make();
        $user->company = Company::factory()->make();
        $response = $this->post('api/users',  $user->toArray(), token_header($this->token));
        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'id'
            ]);

        $user_id = json_decode($response->getContent())->id;
        $company = Company::where('user_id', '=', $user_id)->first();
        $this->assertNotNull($company);
        $this->auth->deleteUser(User::find($user_id)->google_id);
    }


    /**
     * Create an user with billing detail
     *
     * @return void
     */
    public function testCreateUserWithBillingDetail()
    {
        $user = User::factory()->with_billing_detail()->make();
        $user->billing_detail = BillingDetail::factory()->make();
        $user->company = Company::factory()->make();
        $response = $this->post('api/users',  $user->toArray(), token_header($this->token));
        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'id'
            ]);

        $user_id = json_decode($response->getContent())->id;
        $billing_detail = BillingDetail::where('user_id', '=', $user_id)->first();
        $this->assertNotNull($billing_detail);
        $company = Company::where('user_id', '=', $user_id)->first();
        $this->assertNotNull($company);
        $this->auth->deleteUser(User::find($user_id)->google_id);
    }

    /**
     * Create an user with invalid user data
     *
     * @return void
     */
    public function testCreateUserWithInvalidUserData()
    {
        $user = User::factory()->missing_email()->make();
        $user->billing_detail = BillingDetail::factory()->make();
        $user->company = Company::factory()->make();
        $response = $this->post('api/users',  $user->toArray(), token_header($this->token));
        $response
            ->assertStatus(400);
    }

    /**
     * Create an user with invalid company data
     *
     * @return void
     */
    public function testCreateUserWithInvalidCompanyData()
    {
        $user = User::factory()->make();
        $user->billing_detail = BillingDetail::factory()->make();
        $user->company = Company::factory()->missing_name()->make();
        $response = $this->post('api/users',  $user->toArray(), token_header($this->token));
        $response
            ->assertStatus(400)
            ->assertJsonStructure([
                'errors'
            ]);
    }

    /**
     * Create an user with invalid billing data
     *
     * @return void
     */
    public function testCreateUserWithInvalidBillingData()
    {
        $user = User::factory()->with_billing_detail()->make();
        $user->billing_detail = BillingDetail::factory()->missing_name()->make();
        $user->company = Company::factory()->make();
        $response = $this->post('api/users',  $user->toArray(), token_header($this->token));
        $response
            ->assertStatus(400)
            ->assertJsonStructure([
                'errors'
            ]);
    }

    /**
     * Edit email of the user
     *
     * @return void
     */
    public function testEditUserEmail()
    {
        $user = User::factory()->make();
        $user->company = Company::factory()->make();
        $response = $this->post('api/users',  $user->toArray(), token_header($this->token));
        $user_id = json_decode($response->getContent())->id;
        $user->email = 'joaopaulo0503@gmail.com';
        $response = $this->patch('api/users/' . $user_id,  $user->toArray(), token_header($this->token));
        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'email' => 'joaopaulo0503@gmail.com'
            ]);
        $this->auth->deleteUser(User::find($user_id)->google_id);
    }

    /**
     * Edit company name
     *
     * @return void
     */
    public function testEditUserCompany()
    {
        $user = User::factory()->make();
        $user->company = Company::factory()->make();
        $response = $this->post('api/users',  $user->toArray(), token_header($this->token));
        $user_id = json_decode($response->getContent())->id;
        $user->company->name = 'toto consultancy';
        $response = $this->patch('api/users/' . $user_id,  $user->toArray(), token_header($this->token));
        $response
            ->assertStatus(200);
        $content = json_decode($response->getContent());
        $this->assertEquals($content->company->name, $user->company->name);
        $this->auth->deleteUser(User::find($user_id)->google_id);
    }

    /**
     * Edit billing detail name with an existing billing detail
     *
     * @return void
     */
    public function testEditUserBillingDetailThatExist()
    {
        $user = User::factory()->with_billing_detail()->make();
        $user->company = Company::factory()->make();
        $user->billing_detail = BillingDetail::factory()->make();
        $response = $this->post('api/users',  $user->toArray(), token_header($this->token));
        $user_id = json_decode($response->getContent())->id;
        $user->billing_detail->name = 'toto consultancy';
        $response = $this->patch('api/users/' . $user_id,  $user->toArray(), token_header($this->token));
        $response
            ->assertStatus(200);
        $content = json_decode($response->getContent());
        $this->assertEquals($content->billing_detail->name, $user->billing_detail->name);
        $this->auth->deleteUser(User::find($user_id)->google_id);
    }

    /**
     * Edit billing detail name with not an existing billing detail
     *
     * @return void
     */
    public function testEditUserBillingDetailThatNotExist()
    {
        $user = User::factory()->make();
        $user->company = Company::factory()->make();
        $response = $this->post('api/users',  $user->toArray(), token_header($this->token));
        $user_id = json_decode($response->getContent())->id;
        $user->use_company_details_for_billing = false;
        $user->billing_detail = BillingDetail::factory()->make();
        $user->billing_detail->name = 'toto consultancy';
        $response = $this->patch('api/users/' . $user_id,  $user->toArray(), token_header($this->token));
        $response
            ->assertStatus(200);
        $content = json_decode($response->getContent());
        $this->assertEquals($content->billing_detail->name, $user->billing_detail->name);
        $this->auth->deleteUser(User::find($user_id)->google_id);
    }

    /**
     * Delete an user
     *
     * @return void
     */
    public function testDeleteUser()
    {
        $user = User::factory()->make();
        $user->company = Company::factory()->make();
        $response = $this->post('api/users',  $user->toArray(), token_header($this->token));
        $user_id = json_decode($response->getContent())->id;
        $response = $this->delete('api/users/' . $user_id, [], token_header($this->token));
        $response
            ->assertStatus(200);
        $this->assertTrue(
            User::withTrashed()
                ->where('id', $user_id)->first()->trashed()
        );
        $this->auth->deleteUser(User::withTrashed()->where('id', $user_id)->first()->google_id);
    }

    /**
     * Delete that does not exist user
     *
     * @return void
     */
    public function testDeleteUserThatNotExist()
    {
        $response = $this->delete('api/users/' . 1, [], token_header($this->token));
        $response
            ->assertStatus(404);
    }
}
