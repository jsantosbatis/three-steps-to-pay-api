<?php


use App\Mailjet\Mailjet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('test', function () {
    Log::error("It is not working");
    return phpinfo();
});

Route::post('transactions/callback', 'TransactionController@callback');

Route::middleware(['check_token', 'throttle:60,1'])->group(function () {
    Route::apiResources([
        'users' => 'UserController',
        'point_of_sales' => 'PointOfSaleController',
        'products' => 'ProductController',
        'bank_accounts' => 'BankAccountController',
    ]);

    Route::get('users/send_password_reset_link/{user_id}', 'UserController@sendPasswordResetLink');
    Route::get('users/find_by_google_id/{google_id}', 'UserController@findUserByGoogleId');

    Route::get('point_of_sales/{point_of_sale}/products', 'PointOfSaleController@products');
    Route::get('point_of_sales/{point_of_sale}/last_sales', 'PointOfSaleController@lastSales');
    Route::get('point_of_sales/{point_of_sale}/total_sales', 'PointOfSaleController@totalSales');
    Route::get('point_of_sales/{point_of_sale}/total_sales_category', 'PointOfSaleController@totalSalesCategory');
    Route::get('point_of_sales/{point_of_sale}/total_sales_payment_method_type', 'PointOfSaleController@totalSalesPaymentMethodType');
    Route::get('point_of_sales/generate_qr_code/{point_of_sale}', 'PointOfSaleController@generateQRCode');

    Route::get('products/qrcode/{product}', 'ProductController@generateQRCode');
    Route::get('products/qrcode/payconiq/{product}', 'ProductController@generatePayconiqQRCode');

    Route::get('transactions/last_sales', 'TransactionController@lastSales');
    Route::get('transactions/total_sales', 'TransactionController@totalSales');

    Route::get('invoices/{invoice}/download', 'InvoiceController@download');
    Route::get('invoices', 'InvoiceController@index');
    Route::get('invoices/{invoice}/transfers', 'InvoiceController@transfers');

    Route::post('transfers/{transfer}/treated', 'TransferController@treated');

    Route::post('users/reset_password', 'UserController@resetPassword');

    Route::get('product_categories/from_lang/{lang}', 'ProductCategoryController@fromLang');
});

Route::middleware(['throttle:1000,1'])->group(function () {
    Route::post('users/reset_password', 'UserController@resetPassword');
});


Route::middleware(['throttle:10,1'])->group(function () {
    Route::post('contact_form', function (Request $request) {
        $input = $input = $request->all();
        $validator = Validator::make($input, [
            'email' => 'required|email',
            'name' => 'required|string',
            'phone' => '',
            'message' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }

        Mailjet::sendContactForm($input);
    });
});

Route::middleware(['lang', 'throttle:60,1'])->prefix('app')->group(function () {
    Route::get('point_of_sales/find_geo', 'PointOfSaleController@findGeo');
    Route::get('point_of_sales/search', 'PointOfSaleController@search');
    Route::get('point_of_sales/{token}', 'PointOfSaleController@getPointOfSaleFromToken');
    Route::get('products/{token}', 'ProductController@getProductFromToken');
    Route::post('transactions/checkout', 'TransactionController@checkout');
});
