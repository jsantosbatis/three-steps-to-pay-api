<?php

return [
    'merchant_name' => in_array(env('APP_ENV'), ['local', 'testing']) ? 'JoaoPaulo' : 'Jo Consultancy',
    'merchant_id' => in_array(env('APP_ENV'), ['local', 'testing']) ? '5dfc8ebb3c1132000882829c' : '5dee3f6a0c375e0006e438e6',
    'payment_profil_id' => in_array(env('APP_ENV'), ['local', 'testing']) ? '5dfc8ecd3c1132000882829d' : '5dee3f850c375e0006e438e7',
    'api_key' => in_array(env('APP_ENV'), ['local', 'testing']) ? 'c21203eb-ef7d-4d9c-862a-a81ce61c13b8' : '793bf1e9-d49b-44b9-9ba6-027439d03dc8',
    'api_url' => in_array(env('APP_ENV'), ['local', 'testing']) ? 'https://api.ext.payconiq.com/' : 'https://api.payconiq.com/',
    'payconiq_certificate_url' => in_array(env('APP_ENV'), ['local', 'testing']) ? 'https://ext.payconiq.com/certificates' : 'https://payconiq.com/certificates',
    'qr_code_url' => 'https://portal.payconiq.com/qrcode?f={image_format}&s={image_size}&c={payload}',
    'payload_url' => 'https://payconiq.com/t/1/{payment_profil_id}?D={description}&A={amount}&R={reference}',
    'online_payment_fees'  => 20,
    'instore_payment_fees'  => 6,
];
